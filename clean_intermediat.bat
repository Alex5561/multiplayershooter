@echo off

set dirToremove=Intermediatermdir .vs Binaries Saved DerivedDataCache
set FilesToRemove= *.sln

for %%f in (%dirToremove%) do (
    rmdir /s /q %%f
)

for %%f in (%FilesToRemove%) do (
    del  /q %%f
)


