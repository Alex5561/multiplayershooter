// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MultiplayerMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERSESSION_API UMultiplayerMenu : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
		void MenuSetup(int32 PublicConnectionPlayer = 4, FString TypeOfMatch = FString(TEXT("FreeForAll")), FString PatchLobby = FString(TEXT("/Game/ThirdPersonCPP/Maps/Lobby")));

protected:

	virtual bool Initialize() override;
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;


	//CallBack Function Custom Delegate Subsystem

	UFUNCTION()
		void OnCreateSession(bool bSuccsessFull);
		void OnFindSessions(const TArray<FOnlineSessionSearchResult>& SessionResults, bool bWasSuccessful);
		void OnJoinSession(EOnJoinSessionCompleteResult::Type Result);
	UFUNCTION()
		void OnDestroySession(bool bWasSuccessful);
	UFUNCTION()
		void OnStartSession(bool bWasSuccessful);

private:

	UPROPERTY(meta = (BindWidget))
	 class UButton* HostButton;

	UPROPERTY(meta = (BindWidget))
		 UButton* JoinButton;

	UFUNCTION()
	void HostButtonClicked();

	UFUNCTION()
	void JoinButtonClicked();

	class UMultiplayerSessionSubSystem* MultiplayerSessionSubsystem;

	void MenuTearDown();

	int32 NumPublicConnections{4};
	FString MatchType{TEXT("FreeForAll")};
	FString PatchToLobby{TEXT("")};

	
};
