// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterPlayerController.h"
#include "OnlineBlasterCPP/HUD/BlasterHUD.h"
#include "OnlineBlasterCPP/HUD/CharacterOverlay.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "Net/UnrealNetwork.h"
#include "OnlineBlasterCPP/GameMode/BlasterGameMode.h"
#include "OnlineBlasterCPP/HUD/Annoucment.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineBlasterCPP/Components/CombatComponent.h"
#include "OnlineBlasterCPP/GameState/BlasterGameState.h"
#include "OnlineBlasterCPP/PlayerState/BlasterPlayerState.h"
#include "Components/Image.h"
#include "Animation/WidgetAnimation.h"
#include "OnlineBlasterCPP/HUD/ReturnToMainMenu.h"
#include "OnlineBlasterCPP/TurningInPlace.h"

using namespace Announcement;

void ABlasterPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(aPawn);
	if (BlasterCharacter)
	{
		SetHealthBar(BlasterCharacter->GetHealth(), BlasterCharacter->GetMaxHealth());
	}
}

void ABlasterPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	SetHUDTime();

	CheckTimeSync(DeltaSeconds);
	PollInit();
	CheckPing(DeltaSeconds);
	
}


void ABlasterPlayerController::CheckPing(float DeltaTime)
{
	HightPingRunningTime += DeltaTime;
	if (HightPingRunningTime > CheckPingFrequncy)
	{
		PlayerState = PlayerState == nullptr ? GetPlayerState<APlayerState>() : PlayerState;
		if (PlayerState)
		{
			if (PlayerState->GetPingInMilliseconds() /** 4*/ > HighPingThershould)// Пинг сжат , то есть поделен на 4 для клиента/ в новом АПИ пинг возвращает значение уже умноженное, но это не точно
			{
				HighPingWarning();
				PingAnimationRunningTime = 0.f;
				ServerReportPingStatus(true);
			}
			else
			{
				ServerReportPingStatus(false);
			}
		}
		HightPingRunningTime = 0.f;
	}
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->PingAnimation && BlasterHUD->CharacterOverlay->IsAnimationPlaying(BlasterHUD->CharacterOverlay->PingAnimation))
	{
		PingAnimationRunningTime += DeltaTime;
		if (PingAnimationRunningTime > HighPingDuration)
		{
			StopHighPingWarning();
		}
	}
}
//Dead List
void ABlasterPlayerController::BroadcastDead(APlayerState* Attacker, APlayerState* Vicktim)
{
	ClientBroadcastDead(Attacker, Vicktim);
}


//indef Team Score
void ABlasterPlayerController::HideTeamScore()
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->RedTeamScore && BlasterHUD->CharacterOverlay->BlueTeamScore)
	{
		BlasterHUD->CharacterOverlay->RedTeamScore->SetText(FText());
		BlasterHUD->CharacterOverlay->BlueTeamScore->SetText(FText());
	}
}

void ABlasterPlayerController::InitTeamScore()
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->RedTeamScore && BlasterHUD->CharacterOverlay->BlueTeamScore)
	{
		FString Zero{ "0" };
		BlasterHUD->CharacterOverlay->RedTeamScore->SetText(FText::FromString(Zero));
		BlasterHUD->CharacterOverlay->BlueTeamScore->SetText(FText::FromString(Zero));
	}
}

void ABlasterPlayerController::SetHUDRedTeamScore(int32 value)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->RedTeamScore)
	{
		FString RedScore = FString::Printf(TEXT("%d"),value);
		BlasterHUD->CharacterOverlay->RedTeamScore->SetText(FText::FromString(RedScore));
	}
}

void ABlasterPlayerController::SetHUDBlueTeamScore(int32 value)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay  && BlasterHUD->CharacterOverlay->BlueTeamScore)
	{
		FString BlueScore = FString::Printf(TEXT("%d"), value);
		BlasterHUD->CharacterOverlay->BlueTeamScore->SetText(FText::FromString(BlueScore));
	}
}
//end if Team Score


void ABlasterPlayerController::ClientBroadcastDead_Implementation(APlayerState* Attacker, APlayerState* Vicktim)
{

	APlayerState* Self = GetPlayerState<APlayerState>();
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && Attacker && Vicktim && Self)
	{
		BlasterHUD->AddDeadList(Attacker->GetPlayerName(), Vicktim->GetPlayerName());
	}
}

void ABlasterPlayerController::OnRep_bShowTeamScore()
{
	if(bShowTeamScore)
	{
		InitTeamScore();
	}
	else
	{
		HideTeamScore();
	}
}

//End DeadList
void ABlasterPlayerController::ServerReportPingStatus_Implementation(bool bHighPing)
{
	HightPingDelegate.Broadcast(bHighPing);
}

void ABlasterPlayerController::ReceivedPlayer()
{
	Super::ReceivedPlayer();

	if (IsLocalController())
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
	}
}

float ABlasterPlayerController::GetServerTime()
{
	if(HasAuthority()) return GetWorld()->GetTimeSeconds();// Если мы сервер просто возвращаем свое время
	else return GetWorld()->GetTimeSeconds() + ClientServerDelta;// Получаем время сервера  с помощью дельты
}



void ABlasterPlayerController::BeginPlay()
{
	Super::BeginPlay();

	BlasterHUD = Cast<ABlasterHUD>(GetHUD());

	ServerCheckMatchState();
	
}


void ABlasterPlayerController::SetHUDTime()
{
	float TimeLeft = 0.f;
	if(MatchState == MatchState::WaitingToStart) TimeLeft = WarMupTime - GetServerTime() + LevelStartingTime;
	else if(MatchState == MatchState::InProgress) TimeLeft = WarMupTime + MatchTime - GetServerTime() + LevelStartingTime;
	else if(MatchState == MatchState::Colldown)	 TimeLeft = WarMupTime + MatchTime + ColldownTimer - GetServerTime() + LevelStartingTime ;

	uint32 SecondLeft = FMath::CeilToInt(TimeLeft);

	/*if (HasAuthority())
	{
		BlasterGameMode = BlasterGameMode == nullptr ? Cast<ABlasterGameMode>(UGameplayStatics::GetGameMode(this)) : BlasterGameMode;
		if (BlasterGameMode)
		{
			SecondLeft = FMath::CeilToInt(BlasterGameMode->GetCountdownTime() + LevelStartingTime);
		}
	}*/

	if (CountdownInt != SecondLeft)
	{
		if (MatchState == MatchState::WaitingToStart || MatchState == MatchState::Colldown)
		{
			SetHUDAnnocmentTime(TimeLeft);
		}
		if (MatchState == MatchState::InProgress)
		{
			SetHUDMatchTime(TimeLeft);
		}	
	}
	CountdownInt = SecondLeft;
}

void ABlasterPlayerController::PollInit()
{
	if (CharacterOverlay == nullptr)
	{
		if (BlasterHUD && BlasterHUD->CharacterOverlay)
		{
			CharacterOverlay = BlasterHUD->CharacterOverlay;
			if (CharacterOverlay)
			{
				if(bInitializeHealth) SetHealthBar(HUDHealth,MaxHUDHealth);
				if(bInitializeShield) SetShieldBar(HUDShield, MaxHUDShield);
				if(bInitializeScore) SetHUDScore(HUDScore);
				if(bInitializeDefeats) SetHUDDefeats(HUDDefeats);
				if (bInitializeWeaponAmmo) SetHUDAmmoWeapon(HUDWeaponAmmo);
				if (bInitializeCurriedAmmo) SetHUDCurriedAmmo(HUDCurriedAmmo);
				ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(GetPawn());
				if (BlasterCharacter && BlasterCharacter->GetCombatComponent())
				{
					if(bInitilizeGrenade) SetHUDGrenade(BlasterCharacter->GetCombatComponent()->GetGrenadeCount());
				}
				
			}
		}
	}
}


// Ping

void ABlasterPlayerController::HighPingWarning()
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->PingImage && BlasterHUD->CharacterOverlay->PingAnimation)
	{
		BlasterHUD->CharacterOverlay->PingImage->SetOpacity(1.f);
		BlasterHUD->CharacterOverlay->PlayAnimation(BlasterHUD->CharacterOverlay->PingAnimation,0.f,5);
	}
}

void ABlasterPlayerController::StopHighPingWarning()
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->PingImage && BlasterHUD->CharacterOverlay->PingAnimation)
	{
		BlasterHUD->CharacterOverlay->PingImage->SetOpacity(0.f);
		if (BlasterHUD->CharacterOverlay->IsAnimationPlaying(BlasterHUD->CharacterOverlay->PingAnimation))
		{
			BlasterHUD->CharacterOverlay->StopAnimation(BlasterHUD->CharacterOverlay->PingAnimation);
		}
	}
}

//Menu Pause Return MainMenu
void ABlasterPlayerController::ShowReturnToMainMenu()
{
	UE_LOG(LogTemp, Warning, TEXT("Quit"));
	if (!ReturnMainMenuclass) return;

	if (ReturnToMainMenu == nullptr)
	{
		ReturnToMainMenu = CreateWidget<UReturnToMainMenu>(this, ReturnMainMenuclass);
	}
	if (ReturnToMainMenu)
	{
		bReturnToMainMenuOpen = !bReturnToMainMenuOpen;
		if (bReturnToMainMenuOpen)
		{
			ReturnToMainMenu->MenuSetup();
		}
		else
		{
			ReturnToMainMenu->MenuTearDown();
		}
		
	}
	
}

void ABlasterPlayerController::ServerRequestServerTime_Implementation(float TimeOfClientReqest)
{
	float ServerTimeofReceipt = GetWorld()->GetTimeSeconds();
	ClientReportServerTime(TimeOfClientReqest,ServerTimeofReceipt);
}

void ABlasterPlayerController::ClientReportServerTime_Implementation(float TimeOfClientReqest, float TimeServerRequest)
{
	float RoundTripTime = GetWorld()->GetTimeSeconds() - TimeOfClientReqest;//Сколько потребовалось времени на отправку серверу и обратно
	SingleTripTime = 0.5f * RoundTripTime;
	float CurrentServerTime = TimeServerRequest + SingleTripTime;//получаем текущее время на сервере
	ClientServerDelta = CurrentServerTime - GetWorld()->GetTimeSeconds();//Вычитаем дельту Времени клиента от сервера


}

void ABlasterPlayerController::CheckTimeSync(float DeltaTime)
{
	TimeSyncRunningTime += DeltaTime;
	if (IsLocalController() && TimeSyncRunningTime > TimeSyncFrequncity)
	{
		ServerRequestServerTime(GetWorld()->GetTimeSeconds());
		TimeSyncRunningTime = 0.f;
	}
}

void ABlasterPlayerController::ServerCheckMatchState_Implementation()
{
	ABlasterGameMode* BlasterGemeMode = Cast<ABlasterGameMode>(UGameplayStatics::GetGameMode(this));
	if (BlasterGemeMode)
	{
		WarMupTime = BlasterGemeMode->WarMupTime;
		MatchTime = BlasterGemeMode->MatchTime;
		LevelStartingTime = BlasterGemeMode->LevelStartingTime;
		ColldownTimer = BlasterGemeMode->CoolDownTime;
		MatchState = BlasterGemeMode->GetMatchState();
		ClientMidGameJoin(MatchState, WarMupTime, MatchTime, LevelStartingTime, ColldownTimer);

		/*if (BlasterHUD && MatchState == MatchState::WaitingToStart)
		{
			BlasterHUD->AddAnnoucment();
		}*/
	}
}



void ABlasterPlayerController::ClientMidGameJoin_Implementation(FName StateOfMatch, float WapMap, float Match, float StartingTime, float CoolDownTime)
{
	WarMupTime = WapMap;
	MatchTime = Match;
	LevelStartingTime = StartingTime;
	ColldownTimer = CoolDownTime;
	MatchState = StateOfMatch;
	OnMatchStateSet(MatchState);

	if (BlasterHUD && MatchState == MatchState::WaitingToStart)
	{
		BlasterHUD->AddAnnoucment();
	}
}



void ABlasterPlayerController::OnMatchStateSet(FName MatchStateSet, bool bTeamsMatch)
{
	MatchState = MatchStateSet;

	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarting();
	}
	else if (MatchState == MatchState::Colldown)
	{
		HandleColldown();
	}
}



void ABlasterPlayerController::OnRep_MatchState()
{
	if (MatchState == MatchState::InProgress)
	{
		HandleMatchHasStarting();
	}
	else if (MatchState == MatchState::Colldown)
	{
		HandleColldown();
	}
}



void ABlasterPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	InputComponent->BindAction(TEXT("Ouit"), IE_Pressed, this, &ABlasterPlayerController::ShowReturnToMainMenu);
	
}

void ABlasterPlayerController::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABlasterPlayerController,MatchState);
	DOREPLIFETIME(ABlasterPlayerController, bShowTeamScore);
}

void ABlasterPlayerController::SetHealthBar(float Health, float MaxHealth)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->HealthBar && BlasterHUD->CharacterOverlay->HealthText)
	{
		const float HealthPersent = Health/MaxHealth;
		BlasterHUD->CharacterOverlay->HealthBar->SetPercent(HealthPersent);
		FString HealthText = FString::Printf(TEXT("%d/%d"),FMath::CeilToInt(Health),FMath::CeilToInt(MaxHealth));
		BlasterHUD->CharacterOverlay->HealthText->SetText(FText::FromString(HealthText));
	}
	else
	{
		bInitializeHealth = true;
		HUDHealth = Health;
		MaxHUDHealth = MaxHealth;
	}
}

void ABlasterPlayerController::SetShieldBar(float Shield, float MaxShield)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->ShieldBar && BlasterHUD->CharacterOverlay->ShieldText)
	{
		const float ShieldPersent = Shield / MaxShield;
		BlasterHUD->CharacterOverlay->ShieldBar->SetPercent(ShieldPersent);
		FString ShieldText = FString::Printf(TEXT("%d/%d"), FMath::CeilToInt(Shield), FMath::CeilToInt(MaxShield));
		BlasterHUD->CharacterOverlay->ShieldText->SetText(FText::FromString(ShieldText));
	}
	else
	{
		bInitializeShield = true;
		HUDShield = Shield;
		MaxHUDShield = MaxShield;
	}
}

void ABlasterPlayerController::SetHUDScore(float ScoreAmount)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->ScoreAmount)
	{
		FString ScoreText = FString::Printf(TEXT("%d"),FMath::FloorToInt(ScoreAmount));
		BlasterHUD->CharacterOverlay->ScoreAmount->SetText(FText::FromString(ScoreText));
	}
	else
	{
		bInitializeScore = true;
		HUDScore = ScoreAmount;
	}
}

void ABlasterPlayerController::SetHUDDefeats(int32 Defeats)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->DefeatsAmount)
	{
		FString DefeatsText = FString::Printf(TEXT("%d"), Defeats);
		BlasterHUD->CharacterOverlay->DefeatsAmount->SetText(FText::FromString(DefeatsText));
	}
	else
	{
		bInitializeDefeats = true;
		HUDDefeats = Defeats;
	}
}

void ABlasterPlayerController::SetHUDAmmoWeapon(int32 Ammo)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->AmmoAmount)
	{
		FString AmmoText = FString::Printf(TEXT("%d"), Ammo);
		BlasterHUD->CharacterOverlay->AmmoAmount->SetText(FText::FromString(AmmoText));
	}
	else
	{
		bInitializeWeaponAmmo = true;
		HUDWeaponAmmo = Ammo;
	}
}

void ABlasterPlayerController::SetHUDCurriedAmmo(int32 Ammo)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->CurriedAmmoAmount)
	{
		FString CurriedAmmoText = FString::Printf(TEXT("%d"), Ammo);
		BlasterHUD->CharacterOverlay->CurriedAmmoAmount->SetText(FText::FromString(CurriedAmmoText));
	}
	else
	{
		bInitializeCurriedAmmo = true;
		HUDCurriedAmmo = Ammo;
	}
}

void ABlasterPlayerController::SetHUDMatchTime(float Time)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->MatchTime)
	{
		if (Time < 0.f)
		{
			BlasterHUD->CharacterOverlay->MatchTime->SetText(FText());
			return;
		}
		int32 Minutes = FMath::FloorToInt(Time / 60);
		int32 Seconds = Time - Minutes * 60.f;
		FString TimeText = FString::Printf(TEXT("%02d:%02d"), Minutes,Seconds);
		BlasterHUD->CharacterOverlay->MatchTime->SetText(FText::FromString(TimeText));
	}
}

void ABlasterPlayerController::SetHUDAnnocmentTime(float Time)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->Annoucment && BlasterHUD->Annoucment->WarMupTime)
	{
		if (Time < 0.f)
		{
			BlasterHUD->Annoucment->WarMupTime->SetText(FText());
			return;
		}
			
		int32 Minutes = FMath::FloorToInt(Time / 60);
		int32 Seconds = Time - Minutes * 60.f;
		FString TimeText = FString::Printf(TEXT("%02d:%02d"), Minutes, Seconds);
		BlasterHUD->Annoucment->WarMupTime->SetText(FText::FromString(TimeText));
	}
}

void ABlasterPlayerController::SetHUDGrenade(int32 CountGrenade)
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay && BlasterHUD->CharacterOverlay->GrenadeText)
	{
		FString CurriedGrenadeText = FString::Printf(TEXT("%d"), CountGrenade);
		BlasterHUD->CharacterOverlay->GrenadeText->SetText(FText::FromString(CurriedGrenadeText));
	}
	else
	{
		bInitilizeGrenade = false;
		HUDGrenade = CountGrenade;
	}
}

void ABlasterPlayerController::HandleMatchHasStarting(bool bTeamsMatch)
{
	if(HasAuthority()) bShowTeamScore = bTeamsMatch;
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD)
	{
		BlasterHUD->AddCHaracterOverlay();
		if (BlasterHUD->Annoucment)
		{
			BlasterHUD->Annoucment->SetVisibility(ESlateVisibility::Collapsed);
		}
		if (!HasAuthority()) return;
		if (bTeamsMatch)
		{
			InitTeamScore();
		}
		else
		{
			HideTeamScore();
		}
	}
}

void ABlasterPlayerController::HandleColldown()
{
	BlasterHUD = BlasterHUD == nullptr ? Cast<ABlasterHUD>(GetHUD()) : BlasterHUD;
	if (BlasterHUD && BlasterHUD->CharacterOverlay)
	{
		BlasterHUD->CharacterOverlay->RemoveFromParent();
		if (BlasterHUD->Annoucment->Annoucment && BlasterHUD->Annoucment->InfoText)
		{
			BlasterHUD->Annoucment->SetVisibility(ESlateVisibility::Visible);
			FString AmmoucmentText = NewMatchStartsIn;//использование пространства имен
			BlasterHUD->Annoucment->Annoucment->SetText(FText::FromString(AmmoucmentText));

			ABlasterGameState* BlasterGameState = Cast<ABlasterGameState>(UGameplayStatics::GetGameState(this));
			ABlasterPlayerState* BlasterPlayerState = GetPlayerState<ABlasterPlayerState>();
			FString InfoTextString;
			if (BlasterGameState && BlasterPlayerState)
			{
				TArray<ABlasterPlayerState*> TopPlayers = BlasterGameState->TopScoringPlayers;
				if (TopPlayers.Num() == 0)
				{
					InfoTextString = Announcement::ThereIsNoWinner;
				}
				/*else if (TopPlayers.Num() == 1 && TopPlayers[0 == BlasterPlayerState])
				{
					InfoTextString = FString("You are the winner!");
				}*/
				else if (TopPlayers.Num() == 1)
				{
					InfoTextString = FString::Printf(TEXT("Winner: \n %s"),* TopPlayers[0]->GetPlayerName());
				}
				else if (TopPlayers.Num() > 1)
				{
					InfoTextString = FString("Players tied for the win:\n");
					for (auto TiedPlayer : TopPlayers)
					{
						InfoTextString.Append(FString::Printf(TEXT("%s\n"), *TiedPlayer->GetPlayerName()));
					}
				}
				BlasterHUD->Annoucment->InfoText->SetText(FText::FromString(InfoTextString));
			}	
		}
	}
	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(GetPawn());
	if (BlasterCharacter && BlasterCharacter->GetCombatComponent())
	{
		BlasterCharacter->bDisableGamePlay = true;
		BlasterCharacter->GetCombatComponent()->FireButtonPressed(false);
	}
}


