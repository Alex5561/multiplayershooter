// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BlasterPlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHightPingDelegate, bool, bPingTooHigh);

UCLASS()
class ONLINEBLASTERCPP_API ABlasterPlayerController : public APlayerController
{
	GENERATED_BODY()
	

public:

	

	virtual void GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const override;
	void SetHealthBar(float Health,float MaxHealth);
	void SetShieldBar(float Shield, float MaxShield);
	void SetHUDScore(float ScoreAmount);
	void SetHUDDefeats(int32 Defeats);
	void SetHUDAmmoWeapon(int32 Ammo);
	void SetHUDCurriedAmmo(int32 Ammo);
	void SetHUDMatchTime(float Time);
	void SetHUDAnnocmentTime(float Time);
	void SetHUDGrenade(int32 CountGrenade);


	virtual void OnPossess(APawn* aPawn) override;
	virtual void Tick( float DeltaSeconds ) override;
	virtual void ReceivedPlayer() override;

	virtual float GetServerTime();

	void OnMatchStateSet(FName MatchStateSet, bool bTeamsMatch = false);
	void HandleMatchHasStarting(bool bTeamsMatch = false);
	void HandleColldown();

	void BroadcastDead(APlayerState* Attacker, APlayerState* Vicktim);


	float SingleTripTime = 0.f;


	FHightPingDelegate HightPingDelegate;

	//Team Scores
	void HideTeamScore();
	void InitTeamScore();
	void SetHUDRedTeamScore(int32 value);
	void SetHUDBlueTeamScore(int32 value);

protected:

	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

	void SetHUDTime();

	void PollInit();

	void HighPingWarning();
	void StopHighPingWarning();

	//Return Main Menu
	void ShowReturnToMainMenu();
	UPROPERTY(EditDefaultsOnly,Category = "HUD")
	TSubclassOf<class UUserWidget> ReturnMainMenuclass;
	UPROPERTY()
		class UReturnToMainMenu* ReturnToMainMenu;
	bool bReturnToMainMenuOpen = false;

//Sync Time between Client and Server
	

	//Request Server Time
	UFUNCTION(Server,Reliable)
		void ServerRequestServerTime(float TimeOfClientReqest);
	UFUNCTION(Client,Reliable)
		void ClientReportServerTime(float TimeOfClientReqest, float TimeServerRequest);

		float ClientServerDelta = 0.f;
	UPROPERTY(EditAnywhere)
		float TimeSyncFrequncity = 5.f;

		float TimeSyncRunningTime = 0.f;

		void CheckTimeSync(float DeltaTime);


//Состояние ожидания матча
		UFUNCTION(Server,Reliable)
			void ServerCheckMatchState();
		UFUNCTION(Client,Reliable)
			void ClientMidGameJoin(FName StateOfMatch, float WapMap, float Match,float StartingTime, float CoolDownTime);


		void CheckPing(float DeltaTime);

	UFUNCTION(Client,Reliable)
	void ClientBroadcastDead(APlayerState* Attacker, APlayerState* Vicktim);


	UPROPERTY(ReplicatedUsing = OnRep_bShowTeamScore)
		bool bShowTeamScore = false;

	UFUNCTION()
		void OnRep_bShowTeamScore();

private:
	UPROPERTY()
	class ABlasterHUD* BlasterHUD;
	UPROPERTY()
	class ABlasterGameMode* BlasterGameMode;

	float MatchTime = 0.f;
	float WarMupTime = 0.f;
	float ColldownTimer = 0.f;
	float LevelStartingTime = 0.f;
	uint32 CountdownInt = 0;

	UPROPERTY(ReplicatedUsing = OnRep_MatchState)
	FName MatchState;

	UFUNCTION()
		void OnRep_MatchState();

	UPROPERTY()
		class UCharacterOverlay* CharacterOverlay;

	float HUDHealth;
	float MaxHUDHealth;
	float HUDScore;
	int32 HUDDefeats;
	int32 HUDGrenade;
	float HUDShield;
	float MaxHUDShield;
	float HUDCurriedAmmo;
	float HUDWeaponAmmo;

	bool bInitializeHealth = false;
	bool bInitializeShield = false;
	bool bInitializeDefeats = false;
	bool bInitializeScore = false;
	bool bInitilizeGrenade = false;
	bool bInitializeCurriedAmmo = false;
	bool bInitializeWeaponAmmo = false;


	float HightPingRunningTime = 0.f;
	float HighPingDuration = 5.f;
	float CheckPingFrequncy = 20.f;
	float HighPingThershould = 50.f;
	float PingAnimationRunningTime = 0.f;

	UFUNCTION(Server,Reliable)
		void ServerReportPingStatus(bool bHighPing);
};
