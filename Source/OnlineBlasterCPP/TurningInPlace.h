#pragma once
//#include "TurninginPlace.generated.h"


UENUM(BlueprintType)
enum class ETurninginPlace : uint8
{
	ETP_Left UMETA(DisplayName = "Left"),
	ETP_Right UMETA(DisplayName = "Right"),
	ETR_NotTurning UMETA(DisplayName = "NotTurning"),

	ETR_DefaultMax UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EWT_AssaultRifle UMETA(DisplayName = "AssaultRifle"),
	EWT_RocketLauncher UMETA(DisplayName = "RocketLauncher"),
	EWT_Pistol UMETA(DisplayName = "Pistol"),
	EWT_SMG UMETA(DisplayName = "SMG"),
	EWT_ShotGun UMETA(DisplayName = "ShotGun"),
	EWT_SniperRifle UMETA(DisplayName = "SniperRifle"),
	EWT_GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),

	EWT_DefaultMax UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class ECombatState : uint8
{
	ECS_Unoccupied UMETA(DisplayName = "Unoccupied"),
	ECS_Reloading UMETA(DisplayName = "Reloading"),
	ECS_TrowhGrenade UMETA(DisplayName = "TrowhGrenade"),
	ECS_SwapWeapon UMETA(DisplayName = "SwapWeapon"),


	ECS_DefaultMax UMETA(DisplayName = "DefaultMax")
};

UENUM(BlueprintType)
enum class ETeam : uint8 
{
	ET_ReadTeam UMETA(DisplayName = "ReadTeam"),
	ET_BlueTeam UMETA(DisplayName = "BlueTeam"),
	ET_NoTeam UMETA(DisplayName = "NoTeam"),

	ET_DefaultMAX UMETA(DisplayName = "DefaultMax")
};


namespace Announcement
{
	const FString NewMatchStartsIn(TEXT("New match starts in:"));
	const FString ThereIsNoWinner(TEXT("There is no winner."));
	const FString YouAreTheWinner(TEXT("You are the winner!"));
	const FString PlayersTiedForTheWin(TEXT("Players tied for the win:"));
	const FString TeamsTiedForTheWin(TEXT("Teams tied for the win:"));
	const FString RedTeam(TEXT("Red team"));
	const FString BlueTeam(TEXT("Blue team"));
	const FString RedTeamWins(TEXT("Red team wins!"));
	const FString BlueTeamWins(TEXT("Blue team wins!"));
}