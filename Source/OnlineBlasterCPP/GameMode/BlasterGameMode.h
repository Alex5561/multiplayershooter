// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "BlasterGameMode.generated.h"


namespace MatchState
{
	extern ONLINEBLASTERCPP_API const FName Colldown; //end Match and Waiting To Start
}

UCLASS()
class ONLINEBLASTERCPP_API ABlasterGameMode : public AGameMode
{
	GENERATED_BODY()
	

public:

	ABlasterGameMode();

	void PlayerLeftGame(class ABlasterPlayerState* LeavingPlayerState);

	virtual void PlayerEliminate(class ABlasterCharacter* ElimmedCharacter, class ABlasterPlayerController* VictimController, ABlasterPlayerController* AttackerController);
	virtual void RequestRespawns(ACharacter* ElimmedCharacter, AController* ElimmedController);

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly)
	float WarMupTime = 10.f;

	UPROPERTY(EditDefaultsOnly)
	float MatchTime = 600.f;

	UPROPERTY(EditDefaultsOnly)
	float CoolDownTime = 25.f;

	float LevelStartingTime = 0.f;

	virtual float CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage);

protected:
	
	virtual void BeginPlay() override;
	virtual void OnMatchStateSet() override;

	bool bTeamsMatchState = false;
	

private:
	float CountDownTime = 0.f;
	
public:

	FORCEINLINE float GetCountdownTime() const { return CoolDownTime; }
};
