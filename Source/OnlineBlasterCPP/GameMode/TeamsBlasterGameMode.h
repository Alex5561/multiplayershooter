// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BlasterGameMode.h"
#include "TeamsBlasterGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API ATeamsBlasterGameMode : public ABlasterGameMode
{
	GENERATED_BODY()
public:
	ATeamsBlasterGameMode();

	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	virtual float CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage) override;
	virtual void PlayerEliminate(class ABlasterCharacter* ElimmedCharacter, class ABlasterPlayerController* VictimController, ABlasterPlayerController* AttackerController) override;
protected:

	virtual void HandleMatchHasStarted() override;
	
};
