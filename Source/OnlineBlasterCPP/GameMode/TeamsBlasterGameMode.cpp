// Fill out your copyright notice in the Description page of Project Settings.


#include "TeamsBlasterGameMode.h"
#include "OnlineBlasterCPP/GameState/BlasterGameState.h"
#include "OnlineBlasterCPP/PlayerState/BlasterPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"

ATeamsBlasterGameMode::ATeamsBlasterGameMode()
{
	bTeamsMatchState = true;
}

void ATeamsBlasterGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	ABlasterGameState* BGameState = Cast<ABlasterGameState>(UGameplayStatics::GetGameState(this));
	if (!BGameState) return;
	ABlasterPlayerState* BPlayerState = NewPlayer->GetPlayerState<ABlasterPlayerState>();
	if (BPlayerState && BPlayerState->GetTeam() == ETeam::ET_NoTeam)
	{
		if (BGameState->BlueTeam.Num() >= BGameState->RedTeam.Num())
		{
			BGameState->RedTeam.AddUnique(BPlayerState);
			BPlayerState->SetTeam(ETeam::ET_ReadTeam);
		}
		else
		{
			BGameState->BlueTeam.AddUnique(BPlayerState);
			BPlayerState->SetTeam(ETeam::ET_BlueTeam);
		}
	}
	
}

void ATeamsBlasterGameMode::Logout(AController* Exiting)
{
	Super::Logout(Exiting);

	ABlasterGameState* BGameState = Cast<ABlasterGameState>(UGameplayStatics::GetGameState(this));
	ABlasterPlayerState* BPlayerState = Exiting->GetPlayerState<ABlasterPlayerState>();
	if (BPlayerState && BGameState)
	{
		if (BGameState->RedTeam.Contains(BPlayerState))
		{
			BGameState->RedTeam.Remove(BPlayerState);
		}
		if (BGameState->BlueTeam.Contains(BPlayerState))
		{
			BGameState->BlueTeam.Remove(BPlayerState);
		}
	}

}



void ATeamsBlasterGameMode::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	ABlasterGameState* BGameState = Cast<ABlasterGameState>(UGameplayStatics::GetGameState(this));
	if (!BGameState) return;
	
	for (auto PState : BGameState->PlayerArray)
	{
		ABlasterPlayerState* BPlayerState = Cast<ABlasterPlayerState>(PState.Get());
		if (BPlayerState && BPlayerState->GetTeam() == ETeam::ET_NoTeam)
		{
			if (BGameState->BlueTeam.Num() >= BGameState->RedTeam.Num())
			{
				BGameState->RedTeam.AddUnique(BPlayerState);
				BPlayerState->SetTeam(ETeam::ET_ReadTeam);
			}
			else
			{
				BGameState->BlueTeam.AddUnique(BPlayerState);
				BPlayerState->SetTeam(ETeam::ET_BlueTeam);
			}
		}

	}
	
}

float ATeamsBlasterGameMode::CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage)
{
	ABlasterPlayerState* AttackerPState = Attacker->GetPlayerState<ABlasterPlayerState>();
	ABlasterPlayerState* VictimPState = Victim->GetPlayerState<ABlasterPlayerState>();
	if (AttackerPState == nullptr || VictimPState == nullptr) return BaseDamage;
	if (VictimPState == AttackerPState)
	{
		return BaseDamage;
	}
	if (AttackerPState->GetTeam() == VictimPState->GetTeam())
	{
		return 0.f;
	}
	return BaseDamage;
}

void ATeamsBlasterGameMode::PlayerEliminate(class ABlasterCharacter* ElimmedCharacter, class ABlasterPlayerController* VictimController, ABlasterPlayerController* AttackerController)
{
	Super::PlayerEliminate(ElimmedCharacter, VictimController, AttackerController);

	ABlasterGameState* BGameState = Cast<ABlasterGameState>(UGameplayStatics::GetGameState(this));
	ABlasterPlayerState* AttackerPState = AttackerController->GetPlayerState<ABlasterPlayerState>();
	if (BGameState && AttackerPState)
	{
		if (AttackerPState->GetTeam() == ETeam::ET_ReadTeam)
		{
			BGameState->RedTeamScores();
		}
		if (AttackerPState->GetTeam() == ETeam::ET_BlueTeam)
		{
			BGameState->BlueTeamScores();
		}
	}
}
