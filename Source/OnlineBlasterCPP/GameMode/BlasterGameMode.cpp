// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterGameMode.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerStart.h"
#include "OnlineBlasterCPP/PlayerState/BlasterPlayerState.h"
#include "OnlineBlasterCPP/GameState/BlasterGameState.h"



namespace MatchState
{
	const FName Colldown = FName("Colldown");
}

ABlasterGameMode::ABlasterGameMode()
{
	bDelayedStart = true;
}

void ABlasterGameMode::PlayerLeftGame(ABlasterPlayerState* LeavingPlayerState)
{
	if (!LeavingPlayerState) return;
	ABlasterGameState* BlasterGameState = GetGameState<ABlasterGameState>();
	if (BlasterGameState && BlasterGameState->TopScoringPlayers.Contains(LeavingPlayerState))
	{
		BlasterGameState->TopScoringPlayers.Remove(LeavingPlayerState);
	}
	ABlasterCharacter* LeavingBlasterCharacter = Cast<ABlasterCharacter>(LeavingPlayerState->GetPawn());
	if (LeavingBlasterCharacter)
	{
		LeavingBlasterCharacter->Elim(true); 
	}
}

void ABlasterGameMode::PlayerEliminate(class ABlasterCharacter* ElimmedCharacter, class ABlasterPlayerController* VictimController, ABlasterPlayerController* AttackerController)
{
	ABlasterPlayerState* AttackerPlayerState = AttackerController ? Cast<ABlasterPlayerState>(AttackerController->PlayerState) : nullptr; 
	ABlasterPlayerState* VicktimPlayerState = AttackerController ? Cast<ABlasterPlayerState>(VictimController->PlayerState) : nullptr;
	ABlasterGameState* BlasterGameState = GetGameState<ABlasterGameState>();

	if (AttackerPlayerState && AttackerPlayerState != VicktimPlayerState && BlasterGameState)
	{
		TArray<ABlasterPlayerState*>PlayersCurrentyInTheLead;
		for (auto LeadPlayer : BlasterGameState->TopScoringPlayers)
		{
			PlayersCurrentyInTheLead.Add(LeadPlayer);
		}

		AttackerPlayerState->AddScore(1.f);
		BlasterGameState->UpdateTopScoring(AttackerPlayerState);

		if (BlasterGameState->TopScoringPlayers.Contains(AttackerPlayerState))
		{
			ABlasterCharacter* Leader = Cast<ABlasterCharacter>(AttackerPlayerState->GetPawn());
			if (Leader)
			{
				Leader->MulticastGainedTheLead();
			}
		}


		for (int32 i{ 0 }; i < PlayersCurrentyInTheLead.Num(); ++i)
		{
			if (!BlasterGameState->TopScoringPlayers.Contains(PlayersCurrentyInTheLead[i]))
			{
				ABlasterCharacter* Loser = Cast<ABlasterCharacter>(PlayersCurrentyInTheLead[i]->GetPawn());
				if (Loser)
				{
					Loser->MulticastLostTheLead();
				}
			}
		}
	}
	if (VicktimPlayerState)
	{
		VicktimPlayerState->AddDefeats(1);
	}

	if(ElimmedCharacter)
	{
		ElimmedCharacter->Elim(false);
	}

	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; ++it)
	{
		ABlasterPlayerController* BlasterPlayerController = Cast<ABlasterPlayerController>(*it);
		if (BlasterPlayerController)
		{
			BlasterPlayerController->BroadcastDead(AttackerPlayerState, VicktimPlayerState);
		}
	}
}

void ABlasterGameMode::RequestRespawns(ACharacter* ElimmedCharacter, AController* ElimmedController)
{
	if (ElimmedCharacter)
	{
		ElimmedCharacter->Reset();
		ElimmedCharacter->Destroy();
	}
	if (ElimmedController)
	{
		TArray<AActor*>PlayerStarts;
		UGameplayStatics::GetAllActorsOfClass(this,APlayerStart::StaticClass(),PlayerStarts);
		int32 Selection = FMath::RandRange(0,PlayerStarts.Num() -1);
		RestartPlayerAtPlayerStart(ElimmedController,PlayerStarts[Selection]);
	}
}

void ABlasterGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (MatchState == MatchState::WaitingToStart)
	{
		CountDownTime = WarMupTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountDownTime <= 0.f)
		{
			StartMatch();
		}
	}
	else if (MatchState == MatchState::InProgress)
	{
		CountDownTime = WarMupTime + MatchTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CountDownTime <= 0.f)
		{
			SetMatchState(MatchState::Colldown);
		}
	}
	else if (MatchState == MatchState::Colldown)
	{
		CountDownTime = WarMupTime + MatchTime + CoolDownTime - GetWorld()->GetTimeSeconds() + LevelStartingTime;
		if (CoolDownTime <= 0.f)
		{
			RestartGame();
		}
	}
}

void ABlasterGameMode::BeginPlay()
{
	Super::BeginPlay();

	LevelStartingTime = GetWorld()->GetTimeSeconds();
}

void ABlasterGameMode::OnMatchStateSet()
{
	Super::OnMatchStateSet();

	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; ++it)
	{
		ABlasterPlayerController* BlasterPlayerController = Cast<ABlasterPlayerController>(*it);
		if (BlasterPlayerController)
		{
			BlasterPlayerController->OnMatchStateSet(MatchState, bTeamsMatchState);
		}
	}
}

float ABlasterGameMode::CalculateDamage(AController* Attacker, AController* Victim, float BaseDamage)
{
	return BaseDamage;
}