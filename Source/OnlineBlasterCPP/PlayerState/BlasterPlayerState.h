// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "OnlineBlasterCPP/TurningInPlace.h"
#include "BlasterPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API ABlasterPlayerState : public APlayerState
{
	GENERATED_BODY()
	

public:

	UFUNCTION()
	virtual void OnRep_Defeats();
	virtual void OnRep_Score() override;
	void AddScore(float AmountScore);
	void AddDefeats(int32 DefeatsAmount);

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;
private:
	UPROPERTY()
	class ABlasterCharacter* Character;
	UPROPERTY()
	class ABlasterPlayerController* Controller;
	UPROPERTY(ReplicatedUsing = OnRep_Defeats)
	int32 Defeats;

	UPROPERTY(ReplicatedUsing = OnRep_Team)
		ETeam Team = ETeam::ET_NoTeam;

	UFUNCTION()
	void OnRep_Team();

public:

	FORCEINLINE ETeam GetTeam() const { return Team; }
	void SetTeam(ETeam value);
};
