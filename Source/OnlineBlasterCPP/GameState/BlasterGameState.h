// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "BlasterGameState.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API ABlasterGameState : public AGameState
{
	GENERATED_BODY()
	

public:

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void UpdateTopScoring(class ABlasterPlayerState* TopScoringPlayerState);

	UPROPERTY(Replicated)
		TArray<ABlasterPlayerState*> TopScoringPlayers;

private:

	float TopScore = 0.f;

public:

	//Team
	void BlueTeamScores();
	void RedTeamScores();

	TArray<ABlasterPlayerState*>RedTeam;
	TArray<ABlasterPlayerState*>BlueTeam;

	UPROPERTY(ReplicatedUsing = OnRep_ReadTeamScore)
		float ReadTeamScore = 0.f;
	UPROPERTY(ReplicatedUsing = OnRep_BlueTeamScore)
		float BlueTeamScore = 0.f;

	UFUNCTION()
		void OnRep_BlueTeamScore();

	UFUNCTION()
		void OnRep_ReadTeamScore();
};
