// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterGameState.h"
#include "Net/UnrealNetwork.h"
#include "OnlineBlasterCPP/PlayerState/BlasterPlayerState.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"

void ABlasterGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABlasterGameState, TopScoringPlayers);
	DOREPLIFETIME(ABlasterGameState, ReadTeamScore);
	DOREPLIFETIME(ABlasterGameState, BlueTeamScore);
}

void ABlasterGameState::UpdateTopScoring(class ABlasterPlayerState* TopScoringPlayerState)
{
	if (TopScoringPlayers.Num() == 0)
	{
		TopScoringPlayers.Add(TopScoringPlayerState);
		TopScore = TopScoringPlayerState->GetScore();
	}
	else if (TopScoringPlayerState->GetScore() == TopScore)
	{
		TopScoringPlayers.AddUnique(TopScoringPlayerState);
	}
	else if (TopScoringPlayerState->GetScore() > TopScore)
	{
		TopScoringPlayers.Empty();
		TopScoringPlayers.AddUnique(TopScoringPlayerState);
		TopScore = TopScoringPlayerState->GetScore();
	}
}

void ABlasterGameState::BlueTeamScores()
{
	++BlueTeamScore;
	ABlasterPlayerController* BController = Cast<ABlasterPlayerController>(GetWorld()->GetFirstPlayerController());
	if (BController)
	{
		BController->SetHUDBlueTeamScore(BlueTeamScore);
	}
}

void ABlasterGameState::RedTeamScores()
{
	++ReadTeamScore;
	ABlasterPlayerController* BController = Cast<ABlasterPlayerController>(GetWorld()->GetFirstPlayerController());
	if (BController)
	{
		BController->SetHUDRedTeamScore(ReadTeamScore);
	}
}

void ABlasterGameState::OnRep_BlueTeamScore()
{
	ABlasterPlayerController* BController = Cast<ABlasterPlayerController>(GetWorld()->GetFirstPlayerController());
	if (BController)
	{
		BController->SetHUDBlueTeamScore(BlueTeamScore);
	}
}

void ABlasterGameState::OnRep_ReadTeamScore()
{
	ABlasterPlayerController* BController = Cast<ABlasterPlayerController>(GetWorld()->GetFirstPlayerController());
	if (BController)
	{
		BController->SetHUDRedTeamScore(ReadTeamScore);
	}
}
