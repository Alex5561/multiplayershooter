// Fill out your copyright notice in the Description page of Project Settings.


#include "ReturnToMainMenu.h"
#include "GameFrameWork/PlayerController.h"
#include "Components/Button.h"
#include "MultiplayerSessionSubSystem.h"
#include "GameFrameWork/GameModeBase.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"



bool UReturnToMainMenu::Initialize()
{
	if (!Super::Initialize())
	{
		return false;
	}
	if (ReturnButton && !ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.AddDynamic(this, &UReturnToMainMenu::ReturnButtonClicked);
	}
	return true;
}

void UReturnToMainMenu::MenuSetup()
{
	AddToViewport();
	SetVisibility(ESlateVisibility::Visible);
	bIsFocusable = true;
	if (!GetWorld()) return;
	PlayerControllerMenu = PlayerControllerMenu== nullptr ? GetWorld()->GetFirstPlayerController() : PlayerControllerMenu;
	if (PlayerControllerMenu)
	{
		FInputModeGameAndUI InputMode;
		InputMode.SetWidgetToFocus(TakeWidget());
		PlayerControllerMenu->SetInputMode(InputMode);
		PlayerControllerMenu->SetShowMouseCursor(true);
	}

	UGameInstance* GameInstance = GetGameInstance();
	if (GameInstance)
	{
		MultiplayerSessionSubSystem = GameInstance->GetSubsystem<UMultiplayerSessionSubSystem>();
		if (MultiplayerSessionSubSystem)
		{
			MultiplayerSessionSubSystem->MultiplayerOnDestroySessionComplete.AddDynamic(this, &UReturnToMainMenu::OnDestroy);
		}
	}
}

void UReturnToMainMenu::MenuTearDown()
{
	RemoveFromParent();
	if (!GetWorld()) return;
	PlayerControllerMenu = PlayerControllerMenu == nullptr ? GetWorld()->GetFirstPlayerController() : PlayerControllerMenu;
	if (PlayerControllerMenu)
	{
		FInputModeGameOnly InputMode;
		PlayerControllerMenu->SetInputMode(InputMode);
		PlayerControllerMenu->SetShowMouseCursor(false);
	}
	if (ReturnButton && ReturnButton->OnClicked.IsBound())
	{
		ReturnButton->OnClicked.RemoveDynamic(this, &UReturnToMainMenu::ReturnButtonClicked);
	}
	if (MultiplayerSessionSubSystem && MultiplayerSessionSubSystem->MultiplayerOnDestroySessionComplete.IsBound())
	{
		MultiplayerSessionSubSystem->MultiplayerOnDestroySessionComplete.RemoveDynamic(this, &UReturnToMainMenu::OnDestroy);
	}
}



void UReturnToMainMenu::PlayerLeftGame()
{
	if (MultiplayerSessionSubSystem)
	{
		MultiplayerSessionSubSystem->DestroySession();
	}
}

void UReturnToMainMenu::ReturnButtonClicked()
{
	ReturnButton->SetIsEnabled(false);
	if (!GetWorld()) return;

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (PlayerController)
	{
		ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(PlayerController->GetPawn());
		if (BlasterCharacter)
		{
			BlasterCharacter->ServerLeftGame();
			BlasterCharacter->OnLeftGame.AddDynamic(this, &UReturnToMainMenu::PlayerLeftGame);
		}
		else
		{
			ReturnButton->SetIsEnabled(true);
		}
	}
	
}

void UReturnToMainMenu::OnDestroy(bool Successfull)
{
	if (!Successfull)
	{
		ReturnButton->SetIsEnabled(true);
		return;
	}

	if (!GetWorld()) return;
	AGameModeBase* GameMode = GetWorld()->GetAuthGameMode<AGameModeBase>();
	if (GameMode)
	{
		GameMode->ReturnToMainMenuHost();
	}
	else
	{
		PlayerControllerMenu = PlayerControllerMenu == nullptr ? GetWorld()->GetFirstPlayerController() : PlayerControllerMenu;
		if (PlayerControllerMenu)
		{
			PlayerControllerMenu->ClientReturnToMainMenuWithTextReason(FText());
		}
	}
}
