// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Annoucment.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API UAnnoucment : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* WarMupTime;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* Annoucment;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* InfoText;

	


	
};
