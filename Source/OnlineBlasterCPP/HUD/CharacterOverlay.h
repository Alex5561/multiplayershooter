// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CharacterOverlay.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API UCharacterOverlay : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(meta = (BindWidget))
		class UProgressBar* HealthBar;

	UPROPERTY(meta = (BindWidget))
		class UProgressBar* ShieldBar;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* HealthText;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ShieldText;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* ScoreAmount;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* DefeatsAmount;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* AmmoAmount;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* CurriedAmmoAmount;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* MatchTime;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* GrenadeText;

	UPROPERTY(meta = (BindWidget))
	class UImage* PingImage;

	UPROPERTY(meta = (BindWidgetAnim),Transient)
	class UWidgetAnimation* PingAnimation;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* BlueTeamScore;

	UPROPERTY(meta = (BindWidget))
		class UTextBlock* RedTeamScore;
};
