// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DeadList.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API UDeadList : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetElimDeadList(FString AttackerName, FString VicktimName);

	UPROPERTY(meta = (BindWidget))
		class UHorizontalBox* DeadBox;
	UPROPERTY(meta = (BindWidget))
		class UTextBlock* DeadText;


};
