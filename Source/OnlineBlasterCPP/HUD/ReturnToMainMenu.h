// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ReturnToMainMenu.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API UReturnToMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:

	void MenuSetup();
	void MenuTearDown();

	UFUNCTION()
	void PlayerLeftGame();

protected:

	virtual bool Initialize() override;

private:

	UPROPERTY(meta = (BindWidget))
		class UButton* ReturnButton;

	UFUNCTION()
	void ReturnButtonClicked();

	UFUNCTION()
	void OnDestroy(bool Successfull);
	
	UPROPERTY()
	class UMultiplayerSessionSubSystem* MultiplayerSessionSubSystem;
	UPROPERTY()
	 class APlayerController* PlayerControllerMenu;
};
