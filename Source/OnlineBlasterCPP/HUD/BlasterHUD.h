// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BlasterHUD.generated.h"

USTRUCT(BlueprintType)
struct FHUDPackage
{
	GENERATED_BODY()

	class UTexture2D* CrossHairCenter;
	UTexture2D* CrossHairLeft;
	UTexture2D* CrossHairRight;
	UTexture2D* CrossHairTop;
	UTexture2D* CrossHairButtom;
	float CrossHairSpread;
	FLinearColor CrossHairColor;

};

UCLASS()
class ONLINEBLASTERCPP_API ABlasterHUD : public AHUD
{
	GENERATED_BODY()

	public:

		virtual void DrawHUD() override;

		UPROPERTY(EditAnywhere,Category = "PlayerStats")
			TSubclassOf<class UUserWidget>CharacterOverlayClass;
		
		UPROPERTY()
		class UCharacterOverlay* CharacterOverlay;

		UPROPERTY(EditAnywhere, Category = "PlayerStats")
			TSubclassOf<class UUserWidget>AnnoucmentClass;

		UPROPERTY()
			class UAnnoucment* Annoucment;

		void AddCHaracterOverlay();
		void AddAnnoucment();
		void AddDeadList(FString Attacker, FString Vicktim);

protected:

	virtual void BeginPlay() override;
	
	UPROPERTY()
		class APlayerController* PlayerControllerOwner;

private:

	FHUDPackage HUDPackage;

	void DrawCrossHair(UTexture2D* Texture,FVector2D ViemportCenter, FVector2D Spread,FLinearColor ColorCrossHair);

	UPROPERTY(EditAnywhere)
		float CrossHairSpreadMax = 16.f;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class UDeadList> DeadListClass;

	UPROPERTY(EditDefaultsOnly)
		float DeadListTime = 1.5f;

	UFUNCTION()
	void DeadListTimerFinish(UDeadList* MsgToText);
	UPROPERTY()
		TArray<UDeadList*>DeadListarr;

public:

	FORCEINLINE void SetHUDPackage(const FHUDPackage& HudPackage) {HUDPackage = HudPackage;}
	
};
