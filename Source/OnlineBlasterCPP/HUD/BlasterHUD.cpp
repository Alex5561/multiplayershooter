// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterHUD.h"
#include "GameFramework/PlayerController.h"
#include "CharacterOverlay.h"
#include "OnlineBlasterCPP/HUD/Annoucment.h"
#include "OnlineBlasterCPP/HUD/DeadList.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Components/HorizontalBox.h"
#include "Components/CanvasPanelSlot.h"


void ABlasterHUD::DrawHUD()
{
	Super::DrawHUD();

	FVector2D ViemportSize;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViemportSize);

		FVector2D ViemportCenter(ViemportSize.X/2.f,ViemportSize.Y / 2.f);

		float CrossHairScale = CrossHairSpreadMax * HUDPackage.CrossHairSpread;

		if (HUDPackage.CrossHairCenter)
		{
			FVector2D Spread(0.f,0.f);
			DrawCrossHair(HUDPackage.CrossHairCenter,ViemportCenter,Spread,HUDPackage.CrossHairColor);
		}
		if (HUDPackage.CrossHairRight)
		{
			FVector2D Spread(CrossHairScale, 0.f);
			DrawCrossHair(HUDPackage.CrossHairRight, ViemportCenter,Spread, HUDPackage.CrossHairColor);
		}
		if (HUDPackage.CrossHairLeft)
		{
			FVector2D Spread(-CrossHairScale, 0.f);
			DrawCrossHair(HUDPackage.CrossHairLeft, ViemportCenter,Spread, HUDPackage.CrossHairColor);
		}
		if (HUDPackage.CrossHairTop)
		{
			FVector2D Spread(0.f,-CrossHairScale);
			DrawCrossHair(HUDPackage.CrossHairTop, ViemportCenter,Spread, HUDPackage.CrossHairColor);
		}
		if (HUDPackage.CrossHairButtom)
		{
			FVector2D Spread(0.f, CrossHairScale);
			DrawCrossHair(HUDPackage.CrossHairButtom, ViemportCenter,Spread, HUDPackage.CrossHairColor);
		}
	}
}



void ABlasterHUD::DrawCrossHair(UTexture2D* Texture, FVector2D ViemportCenter, FVector2D Spread, FLinearColor ColorCrossHair)
{
	const float TextureWidth = Texture->GetSizeX();
	const float TextureHeight = Texture->GetSizeY();
	const FVector2D TextureDrawPoint(ViemportCenter.X - (TextureWidth / 2.f) + Spread.X,ViemportCenter.Y - (TextureHeight / 2.f)+Spread.Y);

	DrawTexture(Texture,TextureDrawPoint.X,TextureDrawPoint.Y,TextureWidth,TextureHeight,0.f,0.f,1.f,1.f, ColorCrossHair);
}


void ABlasterHUD::DeadListTimerFinish(UDeadList* MsgToText)
{
	if (MsgToText)
	{
		MsgToText->RemoveFromParent();
	}
}

void ABlasterHUD::BeginPlay()
{
	Super::BeginPlay();
}

void ABlasterHUD::AddCHaracterOverlay()
{
	APlayerController* PlayerController = GetOwningPlayerController();
	if (PlayerController && CharacterOverlayClass)
	{
		CharacterOverlay = CreateWidget<UCharacterOverlay>(PlayerController,CharacterOverlayClass);
		CharacterOverlay->AddToViewport();
	}
}

void ABlasterHUD::AddAnnoucment()
{
	PlayerControllerOwner = !PlayerControllerOwner ? GetOwningPlayerController() : PlayerControllerOwner;
	if (PlayerControllerOwner && AnnoucmentClass)
	{
		Annoucment = CreateWidget<UAnnoucment>(PlayerControllerOwner, AnnoucmentClass);
		Annoucment->AddToViewport();
	}
}

void ABlasterHUD::AddDeadList(FString Attacker, FString Vicktim)
{
	PlayerControllerOwner = !PlayerControllerOwner ? GetOwningPlayerController() : PlayerControllerOwner;
	if (PlayerControllerOwner && DeadListClass)
	{
		UDeadList* DeadListWidget = CreateWidget<UDeadList>(PlayerControllerOwner, DeadListClass);
		if (DeadListWidget)
		{
			DeadListWidget->SetElimDeadList(Attacker, Vicktim);
			DeadListWidget->AddToViewport();

			for (UDeadList* DeadList : DeadListarr)
			{
				if (DeadList && DeadList->DeadBox)
				{
					UCanvasPanelSlot* CanvasPanelSlot = UWidgetLayoutLibrary::SlotAsCanvasSlot(DeadList->DeadBox);
					if(!CanvasPanelSlot) continue;
					FVector2D CanvasPosition = CanvasPanelSlot->GetPosition();
					FVector2D NewPosition(CanvasPanelSlot->GetPosition().X, CanvasPosition.Y - CanvasPanelSlot->GetSize().Y);
					CanvasPanelSlot->SetPosition(NewPosition);
				}
			}
			DeadListarr.Add(DeadListWidget);

			FTimerHandle MsgTimer;
			FTimerDelegate MsgDelegate;

			MsgDelegate.BindUFunction(this, "DeadListTimerFinish", DeadListWidget);
			GetWorldTimerManager().SetTimer(MsgTimer, MsgDelegate, DeadListTime,false);

		}
	}
}
