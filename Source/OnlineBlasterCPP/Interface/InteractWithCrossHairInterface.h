// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractWithCrossHairInterface.generated.h"


UINTERFACE(MinimalAPI)
class UInteractWithCrossHairInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ONLINEBLASTERCPP_API IInteractWithCrossHairInterface
{
	GENERATED_BODY()

	
public:
};
