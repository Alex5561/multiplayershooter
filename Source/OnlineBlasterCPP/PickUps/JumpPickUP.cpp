// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpPickUP.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "OnlineBlasterCPP/Components/BuffComponent.h"

void AJumpPickUP::OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapSphere(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(OtherActor);
	if (BlasterCharacter)
	{
		UBuffComponent* BuffComponent = Cast<UBuffComponent>(BlasterCharacter->GetBuffComponent());
		if (BuffComponent)
		{
			BuffComponent->BuffJump(JumpZBuff, JumpBuffTime);
		}
	}

	Destroy();
}
