// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUPAmmo.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "OnlineBlasterCPP/Components/CombatComponent.h"



void APickUPAmmo::OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapSphere(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(OtherActor);
	if (BlasterCharacter)
	{
		UCombatComponent* CombatComponent = BlasterCharacter->GetCombatComponent();
		if (CombatComponent)
		{
			CombatComponent->PickUPAmmo(AmmoType, AmmoAmount);
		}
	}

	Destroy();
}
