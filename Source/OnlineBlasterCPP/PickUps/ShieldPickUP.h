// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUP.h"
#include "ShieldPickUP.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API AShieldPickUP : public APickUP
{
	GENERATED_BODY()
	
protected:

	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;


private:

	UPROPERTY(EditAnywhere)
		float ShieldAmount = 55.f;
	UPROPERTY(EditAnywhere)
		float ShieldTime = 5.f;
};
