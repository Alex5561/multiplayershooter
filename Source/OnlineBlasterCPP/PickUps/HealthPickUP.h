// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUP.h"
#include "HealthPickUP.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API AHealthPickUP : public APickUP
{
	GENERATED_BODY()
	
public:

	AHealthPickUP();

protected:

	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	
private:

	UPROPERTY(EditAnywhere)
		float HealthAmount = 35.f;
	UPROPERTY(EditAnywhere)
		float HealingTime = 5.f;

	
};
