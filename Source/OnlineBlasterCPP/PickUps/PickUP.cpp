// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUP.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/StaticMeshComponent.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"


APickUP::APickUP()
{
 	
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	OverlapSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Ovarlap Sphere"));
	OverlapSphere->SetupAttachment(RootComponent);
	OverlapSphere->SetSphereRadius(75.f);
	OverlapSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	OverlapSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	MeshPickUP = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	MeshPickUP->SetupAttachment(OverlapSphere);
	MeshPickUP->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	MeshPickUP->SetRenderCustomDepth(true);
	MeshPickUP->SetCustomDepthStencilValue(251);

	PickUPEmmiter = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Niagara Component"));
	PickUPEmmiter->SetupAttachment(GetRootComponent());
}


void APickUP::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		GetWorldTimerManager().SetTimer(OverlapTimerHandle, this, &APickUP::BindOverlapFinish,OverlapTime);
	}
	
}


void APickUP::OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void APickUP::BindOverlapFinish()
{
	OverlapSphere->OnComponentBeginOverlap.AddDynamic(this, &APickUP::OnOverlapSphere);
}

void APickUP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MeshPickUP)
	{
		MeshPickUP->AddWorldRotation(FRotator(0.f, TurnRateRotate * DeltaTime, 0.f));
	}
}

void APickUP::Destroyed()
{
	Super::Destroyed();

	if (PickUPSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, PickUPSound, GetActorLocation());
	}

	if (DestroyEmmiter)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, DestroyEmmiter, GetActorLocation(), GetActorRotation());
	}
}

