// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUP.h"
#include "OnlineBlasterCPP/TurningInPlace.h"
#include "PickUPAmmo.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API APickUPAmmo : public APickUP
{
	GENERATED_BODY()
	
public:


protected:

	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;


private:

	UPROPERTY(EditAnywhere)
	int32 AmmoAmount = 30;

	UPROPERTY(EditAnywhere)
		EWeaponType AmmoType;
};
