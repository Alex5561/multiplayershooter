// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUP.h"
#include "SpeedPickUP.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API ASpeedPickUP : public APickUP
{
	GENERATED_BODY()
	

public:


protected:

	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;


private:

	UPROPERTY(EditAnywhere)
		float SpeedBuffStand = 1250.f;
	UPROPERTY(EditAnywhere)
		float SpeedBuffCrouch = 600.f;
	UPROPERTY(EditAnywhere)
		float SpeedBuffTime = 25.f;
};
