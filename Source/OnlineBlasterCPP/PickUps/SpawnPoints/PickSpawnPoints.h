// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickSpawnPoints.generated.h"

UCLASS()
class ONLINEBLASTERCPP_API APickSpawnPoints : public AActor
{
	GENERATED_BODY()
	
public:	
	
	APickSpawnPoints();
	virtual void Tick(float DeltaTime) override;
protected:
	
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<class APickUP>> PickUPClasses;

	UPROPERTY()
		APickUP* SpawnPickUPvariable;

private:

	void SpawnPickUP();
	FTimerHandle HandleSpawnTimer;

	UPROPERTY(EditAnywhere)
		float SpawnTime = 10.f;
	void SpawnFinishTimer();
	UFUNCTION()
	void StratTimerRespawnPickUP(AActor* DestoryedActor);
	


};
