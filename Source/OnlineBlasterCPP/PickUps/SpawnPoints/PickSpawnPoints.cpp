// Fill out your copyright notice in the Description page of Project Settings.


#include "PickSpawnPoints.h"
#include "OnlineBlasterCPP/PickUps/PickUP.h"


APickSpawnPoints::APickSpawnPoints()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
}


void APickSpawnPoints::BeginPlay()
{
	Super::BeginPlay();
	
	StratTimerRespawnPickUP((AActor*)nullptr);

}


void APickSpawnPoints::SpawnPickUP()
{
	int32 NumberArray = PickUPClasses.Num();

	if (NumberArray > 0)
	{
		int32 SpawnArrayElemnt = FMath::RandRange(0, NumberArray - 1);
		SpawnPickUPvariable = GetWorld()->SpawnActor<APickUP>(PickUPClasses[SpawnArrayElemnt], GetActorTransform());
		if (HasAuthority() && SpawnPickUPvariable)
		{
			SpawnPickUPvariable->OnDestroyed.AddDynamic(this, &APickSpawnPoints::StratTimerRespawnPickUP);
		}
	}
}

void APickSpawnPoints::SpawnFinishTimer()
{
	if (HasAuthority())
	{
		SpawnPickUP();
	}
}

void APickSpawnPoints::StratTimerRespawnPickUP(AActor* DestoryedActor)
{
	GetWorldTimerManager().SetTimer(HandleSpawnTimer, this, &APickSpawnPoints::SpawnFinishTimer, SpawnTime);
}

void APickSpawnPoints::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

