// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PickUP.h"
#include "JumpPickUP.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API AJumpPickUP : public APickUP
{
	GENERATED_BODY()
	
public:


protected:

	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;


private:

	UPROPERTY(EditAnywhere)
		float JumpZBuff = 4000.f;

	UPROPERTY(EditAnywhere)
		float JumpBuffTime= 25.f;

};
