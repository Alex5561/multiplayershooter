// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PickUP.generated.h"

UCLASS()
class ONLINEBLASTERCPP_API APickUP : public AActor
{
	GENERATED_BODY()
	
public:	
	
	APickUP();

	virtual void Tick(float DeltaTime) override;
	virtual void Destroyed() override;
protected:
	
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void OnOverlapSphere(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	UPROPERTY(EditAnywhere)
		float TurnRateRotate = 45.f;


private:

	UPROPERTY(EditAnywhere)
		class USphereComponent* OverlapSphere;

	UPROPERTY(EditAnywhere)
		class USoundCue* PickUPSound;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* MeshPickUP;
	
	UPROPERTY(VisibleAnywhere)
		class UNiagaraComponent* PickUPEmmiter;

	UPROPERTY(EditAnywhere)
		class UNiagaraSystem* DestroyEmmiter;


	FTimerHandle OverlapTimerHandle;

	UPROPERTY(EditAnywhere)
		float OverlapTime = 0.25f;

	void BindOverlapFinish();
};
