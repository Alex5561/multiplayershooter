// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

UCLASS()
class ONLINEBLASTERCPP_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AProjectile();

	//Server Request

	bool bUseServerRewaid = false;
	FVector_NetQuantize TraceStart;
	FVector_NetQuantize100 InitialVelocity;

	//Переменная для изменения в редакторе вызываеться в функции PostEditChangeProperty, которая определена в классе Projectile Bullet
	UPROPERTY(EditAnywhere)
		float InitialSpeedProjectile = 15000.f;

	//**********************************************

	//Grenade Rocket
	UPROPERTY(EditAnywhere)
		float Damage = 0.f;
	//Grenade Rocket not
	UPROPERTY(EditAnywhere)
		float HeadShotDamage = 0.f;

protected:
	
	
	virtual void BeginPlay() override;

	virtual void Destroyed() override;


	UPROPERTY(EditAnywhere)
		UParticleSystem* ImpactParticle;
	UPROPERTY(EditAnywhere)
		class USoundCue* ImpactSound;

	UPROPERTY(EditAnywhere)
		class UBoxComponent* CollisionBox;
	
	UPROPERTY(VisibleAnywhere)
		class UProjectileMovementComponent* ProjectileMovementComponent;


	//Trail System
	UPROPERTY(EditAnywhere)
		class UNiagaraSystem* TrailSystem;
	UPROPERTY()
		class UNiagaraComponent* TrailComponent;


	void SpawnTrailSystem();


	//Destroy Timer

	FTimerHandle DestroyTimer;

	UPROPERTY(EditAnywhere)
		float DestroyTime = 3.f;

	void StartDestroyTimer();
	void DestroyTimerFinish();

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* ProjectileMesh;


	void ExplojenDamage();

public:	
	
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	virtual void Hit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

private:

	UPROPERTY(EditAnywhere)
		class UParticleSystem* Tracer;
	UPROPERTY()
		class UParticleSystemComponent* TracerComponent;
	


	

};
