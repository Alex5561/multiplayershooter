// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "ProjectileRocket.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API AProjectileRocket : public AProjectile
{
	GENERATED_BODY()
	
public:

	AProjectileRocket();

	virtual void Hit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

#if WITH_EDITOR
	void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

	

protected:



	



	UPROPERTY(EditAnywhere)
		class USoundCue* ProjectileLoop;

	UPROPERTY()
	class UAudioComponent* LoopComponent;

	UPROPERTY(EditAnywhere)
		class USoundAttenuation* LoopAttenution;

	UPROPERTY(VisibleAnywhere)
		class URocketMovementComponent* RocketMovementComponent;
};
