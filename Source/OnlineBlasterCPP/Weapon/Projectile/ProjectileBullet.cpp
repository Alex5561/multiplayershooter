// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBullet.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"
#include "OnlineBlasterCPP/Components/LafCompensasionComponent.h"

AProjectileBullet::AProjectileBullet()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->SetIsReplicated(true);
	ProjectileMovementComponent->InitialSpeed = InitialSpeedProjectile;
	ProjectileMovementComponent->MaxSpeed = InitialSpeedProjectile;
}

#if WITH_EDITOR
void AProjectileBullet::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName PropertyName = PropertyChangedEvent.Property != nullptr ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(AProjectileBullet, InitialSpeedProjectile))
	{
		if (ProjectileMovementComponent)
		{
			ProjectileMovementComponent->InitialSpeed = InitialSpeedProjectile;
			ProjectileMovementComponent->MaxSpeed = InitialSpeedProjectile;
		}
	}
}
#endif

void AProjectileBullet::Hit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{

	ABlasterCharacter* OwnerCharacter = Cast<ABlasterCharacter>(GetOwner());
	if (OwnerCharacter)
	{
		ABlasterPlayerController* ControllerOwner = Cast<ABlasterPlayerController>(OwnerCharacter->Controller);
		if (ControllerOwner)
		{
			if (OwnerCharacter->HasAuthority() && !bUseServerRewaid)
			{
				const float CurrentDamage = Hit.BoneName.ToString() == FString("head") ? HeadShotDamage : Damage;
				UGameplayStatics::ApplyDamage(OtherActor, CurrentDamage, ControllerOwner, this, UDamageType::StaticClass());
				Super::Hit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);
				return;
			}
			ABlasterCharacter* HitCharacter = Cast<ABlasterCharacter>(OtherActor);
			if (bUseServerRewaid && OwnerCharacter->GetLagComponent() && OwnerCharacter->IsLocallyControlled() && HitCharacter)
			{
				OwnerCharacter->GetLagComponent()->ProjectileServerScoreRequest(HitCharacter, TraceStart, InitialVelocity, ControllerOwner->GetServerTime() - ControllerOwner->SingleTripTime);
			}


			
		}
	}
	

	Super::Hit(HitComponent,OtherActor,OtherComp,NormalImpulse, Hit);
}

void AProjectileBullet::BeginPlay()
{
	Super::BeginPlay();

}
