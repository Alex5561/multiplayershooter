// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CasingProjectile.generated.h"

UCLASS()
class ONLINEBLASTERCPP_API ACasingProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	
	ACasingProjectile();

protected:
	
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void Hit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* CasingMesh;
	UPROPERTY(EditAnywhere)
		float ShellEjectorImpulse;
	UPROPERTY(EditAnywhere)
		class USoundCue* ShellSound;

public:	
	

};
