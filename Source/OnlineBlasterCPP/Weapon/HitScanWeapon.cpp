// Fill out your copyright notice in the Description page of Project Settings.


#include "HitScanWeapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/KismetMathLibrary.h"
#include "OnlineBlasterCPP/Components/LafCompensasionComponent.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"



void AHitScanWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);
	

	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (PawnOwner == nullptr) return;
	AController* ControllerInstigator = PawnOwner->GetController();

	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	if (MuzzleSocket)
	{
		FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
		FVector Start = SocketTransform.GetLocation();
		FHitResult HitResult;
		WeaponTraceHit(Start, HitTarget, HitResult);

		
		ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(HitResult.GetActor());
		if (BlasterCharacter && ControllerInstigator)
		{
			bool bCauseAuthDamage = !bUseServerRewaid || PawnOwner->IsLocallyControlled();
			if (HasAuthority() && bCauseAuthDamage)//если не использовать лаг компенсациб то просто нанести урон на стороне сервера
			{
				const float CurrentDamage = HitResult.BoneName.ToString() == FString("head") ? HeadShotDamage : Damage;
				UGameplayStatics::ApplyDamage(BlasterCharacter, CurrentDamage, ControllerInstigator, this, UDamageType::StaticClass());
			}
			else if (!HasAuthority() && bUseServerRewaid)//LagCompensation
			{
				OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(PawnOwner) : OwnerBlasterCHaracter;
				OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(ControllerInstigator) : OwnerBlasterPlayerController;
				if (OwnerBlasterCHaracter && OwnerBlasterPlayerController && OwnerBlasterCHaracter->GetLagComponent() && OwnerBlasterCHaracter->IsLocallyControlled())
				{
					OwnerBlasterCHaracter->GetLagComponent()->ServerScoreRequest(BlasterCharacter, Start, HitTarget, OwnerBlasterPlayerController->GetServerTime() - OwnerBlasterPlayerController->SingleTripTime);
				}
			}
		}
		if (HitSound)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint);
		}
		if (ParticleImpact)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleImpact, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation());
		}

		if (ParticleMuzzle)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleMuzzle, SocketTransform);
		}
		if (FireSound)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), FireSound, SocketTransform.GetLocation());
		}
	}
	
}



void AHitScanWeapon::WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit)
{
	UWorld* World = GetWorld();
	if (World)
	{
		FVector End = TraceStart + (HitTarget - TraceStart) * 1.25f;
		World->LineTraceSingleByChannel(OutHit, TraceStart, End, ECollisionChannel::ECC_Visibility);
		FVector BeamEnd = End;
		if (OutHit.bBlockingHit)
		{
			BeamEnd = OutHit.ImpactPoint;
		}
		if (ParticleBeam)
		{
			UParticleSystemComponent* BeamComponent = UGameplayStatics::SpawnEmitterAtLocation(World, ParticleBeam, TraceStart,FRotator::ZeroRotator,true);
			if (BeamComponent)
			{
				BeamComponent->SetVectorParameter(TEXT("Target"), BeamEnd);
			}
		}
	}
	
}

