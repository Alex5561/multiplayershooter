// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileWeapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Projectile/Projectile.h"

void AProjectileWeapon::Fire(const FVector& HitTarget)
{
	Super::Fire(HitTarget);

	//if(!HasAuthority()) return;

	APawn* InstigatorPawn = Cast<APawn>(GetOwner());
	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName(FName("MuzzleFlash"));
	UWorld* World = GetWorld();
	if (MuzzleSocket && World)
	{
		FTransform TransformSocket = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
		FVector ToTarget = HitTarget - TransformSocket.GetLocation();
		FRotator TargetRotation = ToTarget.Rotation();

		FActorSpawnParameters SpawnParam;
		SpawnParam.Owner = GetOwner();
		SpawnParam.Instigator = InstigatorPawn;


		AProjectile* SpawnProjectile = nullptr;

		if (bUseServerRewaid)//Если включена перемотка на сервере
		{
			if (InstigatorPawn->HasAuthority())// Если это сервер
			{
				if (InstigatorPawn->IsLocallyControlled() && ProjectileClass)//Если мы на сервере и локальный игрок т.е Host то мы просто спавним реплицированный снаряд, т.к. у хоста не может быть задержки
				{
					SpawnProjectile = World->SpawnActor<AProjectile>(ProjectileClass, TransformSocket.GetLocation(), TargetRotation, SpawnParam);
					if (!SpawnProjectile) return;
					SpawnProjectile->Damage = Damage;
					SpawnProjectile->HeadShotDamage = HeadShotDamage;
					SpawnProjectile->bUseServerRewaid = false;

				}
				else// Если мы на сервере и не контролируем локально, значит мы клиент, и будем спавнить снаряд который не реплицируеться
				{
					if (!ServerSideRewindProjectileClass) return;
					SpawnProjectile = World->SpawnActor<AProjectile>(ServerSideRewindProjectileClass, TransformSocket.GetLocation(), TargetRotation, SpawnParam);
					if (!SpawnProjectile) return;
					SpawnProjectile->bUseServerRewaid = true;
				}
			}
			else// Если мы  не на сервере то мы клиент и будем использовать реплицируемый снаряд
			{
				if (InstigatorPawn->IsLocallyControlled())//Спавн не реплицированного снаряда, и задействование отправки перемотки на сервере
				{
					if (!ServerSideRewindProjectileClass) return;
					SpawnProjectile = World->SpawnActor<AProjectile>(ServerSideRewindProjectileClass, TransformSocket.GetLocation(), TargetRotation, SpawnParam);
					if (!SpawnProjectile) return;
					SpawnProjectile->bUseServerRewaid = true;
					SpawnProjectile->TraceStart = TransformSocket.GetLocation();
					SpawnProjectile->InitialVelocity = SpawnProjectile->GetActorForwardVector() * SpawnProjectile->InitialSpeedProjectile;

				}
				else// если это не локальный контроллер то создать снаряд не реплицируемый и не отправлять SSR
				{
					if (!ServerSideRewindProjectileClass) return;
					SpawnProjectile = World->SpawnActor<AProjectile>(ServerSideRewindProjectileClass, TransformSocket.GetLocation(), TargetRotation, SpawnParam);
					if (!SpawnProjectile) return;
					SpawnProjectile->bUseServerRewaid = false;
				}
			}
		}
		else//оружие не использует перемотку на сервере
		{
			if (InstigatorPawn->HasAuthority())
			{
				SpawnProjectile = World->SpawnActor<AProjectile>(ProjectileClass, TransformSocket.GetLocation(), TargetRotation, SpawnParam);
				if (!SpawnProjectile) return;
				SpawnProjectile->Damage = Damage;
				SpawnProjectile->bUseServerRewaid = false;
			}
		}
	}
}
