// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "HitScanWeapon.generated.h"

/**
 * 
 */
UCLASS()
class ONLINEBLASTERCPP_API AHitScanWeapon : public AWeapon
{
	GENERATED_BODY()
	
public:


	virtual void Fire(const FVector& HitTarget) override;

	

protected:


	//************************************
	// Method:    WeaponTraceHit
	// FullName:  AHitScanWeapon::WeaponTraceHit
	// Access:    protected 
	// Returns:   void
	// Qualifier:
	// Parameter: const FVector & TraceStart
	// Parameter: const FVector & HitTarget
	// Parameter: FHitResult & OutHit
	//************************************
	void WeaponTraceHit(const FVector& TraceStart, const FVector& HitTarget, FHitResult& OutHit);



	UPROPERTY(EditAnywhere)
		class UParticleSystem* ParticleImpact;

	UPROPERTY(EditAnywhere)
		class UParticleSystem* ParticleBeam;

	UPROPERTY(EditAnywhere)
		class UParticleSystem* ParticleMuzzle;

	UPROPERTY(EditAnywhere)
	class USoundCue* FireSound;

	UPROPERTY(EditAnywhere)
		class USoundCue* HitSound;

	
	
};
