// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Animation/AnimationAsset.h"
#include "Projectile/CasingProjectile.h"
#include "Engine/SkeletalMeshSocket.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"
#include "OnlineBlasterCPP/Components/CombatComponent.h"
#include "Kismet/KismetMathLibrary.h"


AWeapon::AWeapon()
{
 	
	PrimaryActorTick.bCanEverTick = false;

	bReplicates = true;
	SetReplicateMovement(true);

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Mesh"));
	SetRootComponent(WeaponMesh);
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCustomDepthStencilValue(250);
	WeaponMesh->MarkRenderStateDirty();
	WeaponCustomDepth(true);

	AreaSphere=CreateDefaultSubobject<USphereComponent>(TEXT("Area Sphere"));
	AreaSphere->SetupAttachment(RootComponent);
	AreaSphere->SetSphereRadius(75.f);
	AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	

	PickUPWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickUP UI"));
	PickUPWidget->SetupAttachment(RootComponent);
}




void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	//if (HasAuthority())
	//{
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		AreaSphere->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
		AreaSphere->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnSphereOverlap);
		AreaSphere->OnComponentEndOverlap.AddDynamic(this, &AWeapon::OnSphereEndOverlap);
	//}
	if (PickUPWidget)
	{
		PickUPWidget->SetVisibility(false);
	}
	
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AWeapon::TraceEndThisScatter(const FVector& HitCross)
{
	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	if (MuzzleSocket == nullptr) return FVector();

	FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
	FVector Start = SocketTransform.GetLocation();

	FVector ToTargetNormalize = (HitCross - Start).GetSafeNormal();
	FVector SphereCenter = Start + ToTargetNormalize * DistanceToSphere;
	FVector RandVector = UKismetMathLibrary::RandomUnitVector() * (FMath::FRandRange(0.f, SphereRadiuse));
	FVector EndLoc = SphereCenter + RandVector;
	FVector ToEndLoc = EndLoc - Start;

	return FVector(Start + ToEndLoc * 80'000.f / ToEndLoc.Size());
}


void AWeapon::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(OtherActor);
	if (BlasterCharacter)
	{
		BlasterCharacter->SetOverlapWeapon(this);
	}
}

void AWeapon::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(OtherActor);
	if (BlasterCharacter)
	{
		BlasterCharacter->SetOverlapWeapon(nullptr);
		
	}
}

void AWeapon::Dropped()
{
	SetWeaponState(EWeaponState::EWS_Dropped);
	FDetachmentTransformRules DetachRules(EDetachmentRule::KeepWorld,true);
	WeaponMesh->DetachFromComponent(DetachRules);
	SetOwner(nullptr);
	OwnerBlasterCHaracter = nullptr;
	OwnerBlasterPlayerController = nullptr;
}





void AWeapon::ClientUpdateAmmo_Implementation(int32 ServerAmmo)
{
	if (HasAuthority()) return;
	Ammo = ServerAmmo;
	--Sequnce;
	Ammo -=Sequnce;
	SetHUDAmmo();
}

void AWeapon::ClientAddAmmo_Implementation(int32 AmmoToAdd)
{
	if (HasAuthority()) return;
	Ammo = FMath::Clamp(Ammo + AmmoToAdd, 0, MagCapacity);
	OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
	if (OwnerBlasterCHaracter && OwnerBlasterCHaracter->GetCombatComponent() && IsFullAmmo())
	{
		OwnerBlasterCHaracter->GetCombatComponent()->JumpToShotGunEnd();
	}
	SetHUDAmmo();
}

void AWeapon::SpendRound()
{
	Ammo = FMath::Clamp(Ammo-1,0,MagCapacity);
	SetHUDAmmo();
	if (HasAuthority())
	{
		ClientUpdateAmmo(Ammo);
	}
	else
	{
		++Sequnce;
	}
}



void AWeapon::AddAmmo(int32 AmmoA)
{
	Ammo = FMath::Clamp(Ammo + AmmoA, 0, MagCapacity);
	SetHUDAmmo();
	ClientAddAmmo(AmmoA);
}

//Weapon State

void AWeapon::SetWeaponState(EWeaponState NewStateWeapon)
{
	WeaponState = NewStateWeapon;
	OnWeaponStateSet();
}

void AWeapon::OnWeaponStateSet()
{
	switch (WeaponState)
	{
	case EWeaponState::EWS_Initial:

		break;
	case EWeaponState::EWS_Equpped:
	OnEquiped();
		break;
	case EWeaponState::EWS_Dropped:
	OnDrop();
		break;
	case EWeaponState::EWS_EquppedSecondary:
	OnEquipedSecondary();
		break;
	case EWeaponState::EWS_DefaultMax:
		break;
	default:
		break;
	}
}

void AWeapon::OnRep_WeaponState()
{
	OnWeaponStateSet();
}

void AWeapon::OnEquiped()
{
	ShowPickUPWidget(false);
	AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (WeaponType == EWeaponType::EWT_SMG)
	{
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WeaponMesh->SetEnableGravity(true);
		WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}
	WeaponCustomDepth(false);

	OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
	if (OwnerBlasterCHaracter && bUseServerRewaid)
	{
		OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(OwnerBlasterCHaracter->Controller) : OwnerBlasterPlayerController;
		if (OwnerBlasterPlayerController && HasAuthority() && !OwnerBlasterPlayerController->HightPingDelegate.IsBound()) //проверяем если мы на сервере и не привязаны к делегату 
		{
			OwnerBlasterPlayerController->HightPingDelegate.AddDynamic(this, &AWeapon::OnPingTooHight);
		}
	}
}

void AWeapon::OnDrop()
{
	if (HasAuthority())
	{
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	WeaponMesh->SetSimulatePhysics(true);
	WeaponMesh->SetEnableGravity(true);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	WeaponMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	WeaponCustomDepth(true);

	OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
	if (OwnerBlasterCHaracter && bUseServerRewaid)
	{
		OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(OwnerBlasterCHaracter->Controller) : OwnerBlasterPlayerController;
		if (OwnerBlasterPlayerController && HasAuthority() && OwnerBlasterPlayerController->HightPingDelegate.IsBound()) //проверяем если мы на сервере и привязаны к делегату  то отписываемся от него
		{
			OwnerBlasterPlayerController->HightPingDelegate.RemoveDynamic(this, &AWeapon::OnPingTooHight);
		}
	}
}


void AWeapon::OnEquipedSecondary()
{
	ShowPickUPWidget(false);
	AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetSimulatePhysics(false);
	WeaponMesh->SetEnableGravity(false);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (WeaponType == EWeaponType::EWT_SMG)
	{
		WeaponMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		WeaponMesh->SetEnableGravity(true);
		WeaponMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}
	WeaponCustomDepth(false);

	OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
	if (OwnerBlasterCHaracter && bUseServerRewaid)
	{
		OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(OwnerBlasterCHaracter->Controller) : OwnerBlasterPlayerController;
		if (OwnerBlasterPlayerController && HasAuthority() && OwnerBlasterPlayerController->HightPingDelegate.IsBound()) //проверяем если мы на сервере и привязаны к делегату  то отписываемся от него
		{
			OwnerBlasterPlayerController->HightPingDelegate.RemoveDynamic(this, &AWeapon::OnPingTooHight);
		}
	}
}

//

bool AWeapon::IsEmpthy()
{
	return Ammo <= 0;
}

bool AWeapon::IsFullAmmo()
{
	return Ammo == MagCapacity;
}



void AWeapon::WeaponCustomDepth(bool bEnabled)
{
	if (WeaponMesh)
	{
		WeaponMesh->SetRenderCustomDepth(bEnabled);
	}
}

void AWeapon::ShowPickUPWidget(bool bShowWidget)
{
	if (PickUPWidget)
	{
		PickUPWidget->SetVisibility(bShowWidget);
	}
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeapon, WeaponState);
	DOREPLIFETIME_CONDITION(AWeapon, bUseServerRewaid,COND_OwnerOnly);

}

void AWeapon::Fire(const FVector& HitTarget)
{
	if (FireAnimation)
	{
		WeaponMesh->PlayAnimation(FireAnimation,false);
	}
	if (CasingClass)
	{
		const USkeletalMeshSocket* AmmoEject = WeaponMesh->GetSocketByName(FName("AmmoEject"));
		if (AmmoEject)
		{
			FTransform TransformSocket = AmmoEject->GetSocketTransform(WeaponMesh);
			
			UWorld* World = GetWorld();
			if (World)
			{
				World->SpawnActor<ACasingProjectile>(CasingClass, TransformSocket.GetLocation(), TransformSocket.GetRotation().Rotator());
			}
			
		}
	}
	SpendRound();
	
	
}

void AWeapon::OnRep_Owner()
{
	Super::OnRep_Owner();
	if (Owner == nullptr)
	{
		OwnerBlasterCHaracter = nullptr;
		OwnerBlasterPlayerController = nullptr;
	}
	else
	{
		OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
		if (OwnerBlasterCHaracter && OwnerBlasterCHaracter->GetEquippedWeapon() && OwnerBlasterCHaracter->GetEquippedWeapon() == this)
		{
			SetHUDAmmo();
		}
	}
}

void AWeapon::SetHUDAmmo()
{
	OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : OwnerBlasterCHaracter;
	if (OwnerBlasterCHaracter)
	{
		OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(OwnerBlasterCHaracter->Controller) : OwnerBlasterPlayerController;
		if (OwnerBlasterPlayerController)
		{
			OwnerBlasterPlayerController->SetHUDAmmoWeapon(Ammo);
		}
	}
}

void AWeapon::OnPingTooHight(bool bPingHight)
{
	bUseServerRewaid = !bPingHight;
}

