// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "OnlineBlasterCPP/TurningInPlace.h"
#include "Weapon.generated.h"

UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Initial UMETA(DisplayName = "Initial"),
	EWS_Equpped UMETA(DisplayName = "Equpped"),
	EWS_EquppedSecondary UMETA(DisplayName = "EquppedSecondary"),
	EWS_Dropped UMETA(DisplayName = "Dropped"),

	EWS_DefaultMax UMETA(DisplayName = "DefaultMax")

};

UENUM(BlueprintType)
enum class EFireType : uint8
{
	EFT_HitScanWeapon UMETA(DisplayName = "HitScanWeapon"),
	EFT_ProjectileWeapon UMETA(DisplayName = "Projectile"),
	EFT_ShotGunWeapon UMETA(DisplayName = "ShotGunWeapon"),

	EFT_DefaultMAX UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class ONLINEBLASTERCPP_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AWeapon();

	void ShowPickUPWidget(bool bShowWidget);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Fire(const FVector& HitTarget);

	virtual void OnRep_Owner() override;

	void SetHUDAmmo();

	UFUNCTION()
	void OnPingTooHight(bool bPingHight);

	//Texture Weapon CrossHair
	UPROPERTY(EditAnywhere, Category = "CrossHair")
		class UTexture2D* CrossHairCenter;
	UPROPERTY(EditAnywhere, Category = "CrossHair")
		UTexture2D* CrossHairLeft;
	UPROPERTY(EditAnywhere, Category = "CrossHair")
		UTexture2D* CrossHairRight;
	UPROPERTY(EditAnywhere, Category = "CrossHair")
		UTexture2D* CrossHairTop;
	UPROPERTY(EditAnywhere, Category = "CrossHair")
		UTexture2D* CrossHairBottom;

	UPROPERTY(EditAnywhere, Category = "Combat")
		float FireDelay = 0.15f;
	UPROPERTY(EditAnywhere, Category = "Combat")
		bool bAutomatic = true;

	void Dropped();

	UPROPERTY(EditAnywhere)
		class USoundCue* EquipSound;

	
	bool bDestroyed = false;

	UPROPERTY(EditAnywhere)
		EFireType FireTypeWeapon;

	FVector TraceEndThisScatter(const FVector& HitCross);

protected:
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void OnWeaponStateSet();
	virtual void OnEquiped();
	virtual void OnDrop();
	virtual void OnEquipedSecondary();
	UFUNCTION()
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	virtual void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY()
		class ABlasterCharacter* OwnerBlasterCHaracter;
	UPROPERTY()
		class ABlasterPlayerController* OwnerBlasterPlayerController;

	UPROPERTY(EditAnywhere)
		float Damage = 15.f;
	UPROPERTY(EditAnywhere)
		float HeadShotDamage = 35.f;

	UPROPERTY(Replicated, EditAnywhere)
	bool bUseServerRewaid = false;

private:
	
	UPROPERTY(VisibleAnywhere,Category = "WeaponProperties")
		class USkeletalMeshComponent* WeaponMesh;
	UPROPERTY(VisibleAnywhere,Category = "WeaponProperties")
		class USphereComponent* AreaSphere;
	UPROPERTY(VisibleAnywhere, Category = "WeaponProperties")
		class UWidgetComponent* PickUPWidget;
	UPROPERTY(ReplicatedUsing = OnRep_WeaponState,VisibleAnywhere, Category = "WeaponProperties")
		EWeaponState WeaponState;
	UPROPERTY(EditAnywhere,Category = "Animation")
		class UAnimationAsset* FireAnimation;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ACasingProjectile> CasingClass;

	UFUNCTION()
		void OnRep_WeaponState();

	
	// Zoom FOV while aiming
	UPROPERTY(EditAnywhere)
		float ZoomedFOV = 45.f;
	UPROPERTY(EditAnywhere)
		float ZoomInterpSpeed = 20.f;

	//Ammo

	UPROPERTY(EditAnywhere)
		int32 Ammo;

	int32 Sequnce = 0;
	
	UFUNCTION(Client,Reliable)
		void ClientUpdateAmmo(int32 ServerAmmo);

	UFUNCTION(Client, Reliable)
		void ClientAddAmmo(int32 AmmoToAdd);

	void SpendRound();

	UPROPERTY(EditAnywhere)
		int32 MagCapacity;

	UPROPERTY(EditAnywhere)
		EWeaponType WeaponType;

public:

	 void SetWeaponState(EWeaponState NewStateWeapon); 
	FORCEINLINE USphereComponent* GetAreaSphere() const {return AreaSphere;}
	FORCEINLINE USkeletalMeshComponent* GetWeaponMesh() const {return WeaponMesh;}
	FORCEINLINE float GetZoomedFOV() const {return ZoomedFOV;}
	FORCEINLINE float GetZoomInterpSpeed() const {return ZoomInterpSpeed;}
	bool IsEmpthy();
	bool IsFullAmmo();
	FORCEINLINE EWeaponType GetWeaponType() const {return WeaponType;}
	FORCEINLINE int32 GetAmmo() const {return Ammo;}
	FORCEINLINE int32 GetMagCapacity() const {return MagCapacity;}
	void AddAmmo(int32 AmmoA);


	void WeaponCustomDepth(bool bEnabled);

	FORCEINLINE float GetDamageWeapon() const { return Damage; }
	FORCEINLINE float GetHeadShotDamageWeapon() const { return HeadShotDamage; }

	//Scatter Weapon

	UPROPERTY(EditAnywhere, Category = "Scatter Weapon")
		float DistanceToSphere = 800.f;
	UPROPERTY(EditAnywhere, Category = "Scatter Weapon")
		float SphereRadiuse = 75.f;
	UPROPERTY(EditAnywhere, Category = "Scatter Weapon")
		bool bUseScatter = false;
};
