// Fill out your copyright notice in the Description page of Project Settings.


#include "ShotGunWeapon.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/KismetMathLibrary.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"
#include "OnlineBlasterCPP/Components/LafCompensasionComponent.h"

//void AShotGunWeapon::Fire(const FVector& HitTarget)
//{
//	AWeapon::Fire(HitTarget);
//
//	APawn* PawnOwner = Cast<APawn>(GetOwner());
//	if (PawnOwner == nullptr) return;
//	AController* ControllerInstigator = PawnOwner->GetController();
//
//	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
//	if (MuzzleSocket)
//	{
//		TMap<ABlasterCharacter*, uint32> HitMap;
//		FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
//		FVector Start = SocketTransform.GetLocation();
//		
//		for (uint32 i = 0; i < NumberBullet; i++)
//		{
//			FHitResult HitResult;
//			WeaponTraceHit(Start, HitTarget, HitResult);
//			ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(HitResult.GetActor());
//			if (BlasterCharacter && HasAuthority())
//			{
//				if (HitMap.Contains(BlasterCharacter))
//				{
//					HitMap[BlasterCharacter]++;
//				}
//				else
//				{
//					HitMap.Emplace(BlasterCharacter, 1);
//				}
//				
//			}
//			if (HitSound)
//			{
//				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint,FRotator::ZeroRotator,0.5f,FMath::FRandRange(-0.5f,0.5f));
//			}
//			if (ParticleImpact)
//			{
//				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleImpact, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation());
//			}
//		}
//		for (auto HitPair : HitMap)
//		{
//			if (HitPair.Key && HasAuthority() && ControllerInstigator)
//			{
//				UGameplayStatics::ApplyDamage(HitPair.Key, Damage * HitPair.Value, ControllerInstigator, this, UDamageType::StaticClass());
//			}
//		}
//		
//	}
//
//}

void AShotGunWeapon::FireShotGun(const TArray<FVector_NetQuantize>& HitTargets)
{
	AWeapon::Fire(FVector());

	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (PawnOwner == nullptr) return;
	AController* ControllerInstigator = PawnOwner->GetController();

	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	if (MuzzleSocket)
	{
		const FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
		const FVector Start = SocketTransform.GetLocation();

		TMap<ABlasterCharacter*, uint32> HitMap;
		TMap<ABlasterCharacter*, uint32> HeadShotHitMap;

		for (FVector_NetQuantize HitTarget : HitTargets)
		{
			FHitResult HitResult;
			WeaponTraceHit(Start, HitTarget, HitResult);
			ABlasterCharacter* BlasterCharacter = Cast<ABlasterCharacter>(HitResult.GetActor());
			if (BlasterCharacter)
			{
				if (HitResult.BoneName.ToString() == FString("head"))
				{
					if (HeadShotHitMap.Contains(BlasterCharacter))
					{
						HeadShotHitMap[BlasterCharacter]++;
					}
					else
					{
						HeadShotHitMap.Emplace(BlasterCharacter, 1);
					}
				}
				else
				{
					if (HitMap.Contains(BlasterCharacter))
					{
						HitMap[BlasterCharacter]++;
					}
					else
					{
						HitMap.Emplace(BlasterCharacter, 1);
					}
				}

				
				if (HitSound)
				{
					UGameplayStatics::SpawnSoundAtLocation(GetWorld(), HitSound, HitResult.ImpactPoint, FRotator::ZeroRotator, 0.5f, FMath::FRandRange(-0.5f, 0.5f));
				}
				if (ParticleImpact)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleImpact, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation());
				}
			}
			
		}
		TArray<ABlasterCharacter*> HitCharacters;
		TMap<ABlasterCharacter*, float> DamageMap;
		for (auto HitPair : HitMap)
		{
			if (HitPair.Key)
			{
				DamageMap.Emplace(HitPair.Key, HitPair.Value * Damage);

				HitCharacters.AddUnique(HitPair.Key);
			}
		}
		for (auto HitHeadPair : HeadShotHitMap)
		{
			
			if (HitHeadPair.Key)
			{
				if (DamageMap.Contains(HitHeadPair.Key))
				{
					DamageMap[HitHeadPair.Key]+=HitHeadPair.Value * HeadShotDamage;
				}
				else
				{
					DamageMap.Emplace(HitHeadPair.Key, HitHeadPair.Value * HeadShotDamage);
				}
				HitCharacters.AddUnique(HitHeadPair.Key);
			}
		}

		

		for (auto DamagePair : DamageMap)
		{
			if (DamagePair.Key && ControllerInstigator)
			{
				bool bCauseAuthDamage = !bUseServerRewaid || PawnOwner->IsLocallyControlled();
				if (HasAuthority() && bCauseAuthDamage)
				{
					UGameplayStatics::ApplyDamage(DamagePair.Key, DamagePair.Value, ControllerInstigator, this, UDamageType::StaticClass());
				}
			}
		}

		if (!HasAuthority() && bUseServerRewaid)
		{
			OwnerBlasterCHaracter = OwnerBlasterCHaracter == nullptr ? Cast<ABlasterCharacter>(PawnOwner) : OwnerBlasterCHaracter;
			OwnerBlasterPlayerController = OwnerBlasterPlayerController == nullptr ? Cast<ABlasterPlayerController>(ControllerInstigator) : OwnerBlasterPlayerController;
			if (OwnerBlasterCHaracter && OwnerBlasterPlayerController && OwnerBlasterCHaracter->GetLagComponent() && OwnerBlasterCHaracter->IsLocallyControlled())
			{
				OwnerBlasterCHaracter->GetLagComponent()->ShotGunServerScoreRequest(HitCharacters,Start,HitTargets,OwnerBlasterPlayerController->GetServerTime() - OwnerBlasterPlayerController->SingleTripTime);
			}
		}
	}

	
}

void AShotGunWeapon::ShotGunTraceEndHitScatter(const FVector& HitTarget, TArray<FVector_NetQuantize>& HitScatterTargets)
{
	const USkeletalMeshSocket* MuzzleSocket = GetWeaponMesh()->GetSocketByName("MuzzleFlash");
	if (MuzzleSocket == nullptr) return;

	const FTransform SocketTransform = MuzzleSocket->GetSocketTransform(GetWeaponMesh());
	const FVector Start = SocketTransform.GetLocation();

	const FVector ToTargetNormalize = (HitTarget - Start).GetSafeNormal();
	const FVector SphereCenter = Start + ToTargetNormalize * DistanceToSphere;
	
	for (uint32 i = 0; i < NumberBullet; ++i)
	{

		const FVector RandVector = UKismetMathLibrary::RandomUnitVector() * (FMath::FRandRange(0.f, SphereRadiuse));
		const FVector EndLoc = SphereCenter + RandVector;
		FVector ToEndLoc = EndLoc - Start;
		ToEndLoc = Start + ToEndLoc * 80'000.f / ToEndLoc.Size();

		HitScatterTargets.Add(ToEndLoc);
	}
}
