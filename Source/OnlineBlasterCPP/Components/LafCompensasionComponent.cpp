// Fill out your copyright notice in the Description page of Project Settings.


#include "LafCompensasionComponent.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineBlasterCPP/Weapon/Weapon.h"
#include "OnlineBlasterCPP/OnlineBlasterCPP.h"



ULafCompensasionComponent::ULafCompensasionComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;

	
}



void ULafCompensasionComponent::BeginPlay()
{
	Super::BeginPlay();

	
	
}

void ULafCompensasionComponent::SaveFramePackage(FFramePackage& Package)
{
	BlasterCharacter = BlasterCharacter == nullptr ? Cast<ABlasterCharacter>(GetOwner()) : BlasterCharacter;
	if (BlasterCharacter)
	{
		Package.Time = GetWorld()->GetTimeSeconds();
		Package.Character = BlasterCharacter;
		for (auto& BoxPair : BlasterCharacter->HitCollisionBoxes)
		{
			FBoxInformation BoxInformation;
			BoxInformation.Location = BoxPair.Value->GetComponentLocation();
			BoxInformation.Rotation = BoxPair.Value->GetComponentRotation();
			BoxInformation.BoxExtent = BoxPair.Value->GetScaledBoxExtent();
			Package.HitBoxInfo.Add(BoxPair.Key, BoxInformation);
		}
	}
}



FServerSideRewindResult ULafCompensasionComponent::ConfirmHit(const FFramePackage& Package, ABlasterCharacter* HitCharacter, const FVector_NetQuantize TraceStart, const FVector_NetQuantize& HitLocation)
{
	if (HitCharacter == nullptr) return FServerSideRewindResult();

	FFramePackage CurrentFrame;

	CacheBoxPosition(HitCharacter, CurrentFrame);
	MoveBoxes(HitCharacter, CurrentFrame);
	ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::NoCollision);

	//Включить коллизию для головного бокса чтобы проверить попали ли мы в голову
	UBoxComponent* HeadBox = HitCharacter->HitCollisionBoxes[FName("head")];
	HeadBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	HeadBox->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);

	FHitResult HitResult;
	const FVector EndTrace = TraceStart + (HitLocation - TraceStart) * 1.25f;
	if (!GetWorld()) return FServerSideRewindResult();
	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, EndTrace, ECC_HitBox);

	if (HitResult.bBlockingHit)//HeadShot
	{
		ResetHitBoxes(HitCharacter, CurrentFrame);
		ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::QueryAndPhysics);
		return FServerSideRewindResult{ true,true };
	}
	else
	{
		for (auto& HitPair : HitCharacter->HitCollisionBoxes)
		{
			if (HitPair.Value != nullptr)
			{
				HitPair.Value->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
				HitPair.Value->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);

			}
		}
		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, EndTrace, ECC_HitBox);
		if (HitResult.bBlockingHit)
		{
			ResetHitBoxes(HitCharacter, CurrentFrame);
			ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::NoCollision);
			return FServerSideRewindResult{ true,false };
		}
	}
	ResetHitBoxes(HitCharacter, CurrentFrame);
	ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::NoCollision);
	return FServerSideRewindResult{ false,false };
}

void ULafCompensasionComponent::MoveBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package)
{
	if (HitCharacter == nullptr) return;

	for (auto& HitPair : HitCharacter->HitCollisionBoxes)
	{
		if (HitPair.Value != nullptr)
		{
			HitPair.Value->SetWorldLocation(Package.HitBoxInfo[HitPair.Key].Location);
			HitPair.Value->SetWorldRotation(Package.HitBoxInfo[HitPair.Key].Rotation);
			HitPair.Value->SetBoxExtent(Package.HitBoxInfo[HitPair.Key].BoxExtent);
		}
	}
}

void ULafCompensasionComponent::CacheBoxPosition(ABlasterCharacter* HitCharacter, FFramePackage& OutFramePackage)
{
	if (HitCharacter == nullptr) return;
	for (auto& HitPair : HitCharacter->HitCollisionBoxes)
	{
		if (HitPair.Value != nullptr)
		{
			FBoxInformation BoxInfo;
			BoxInfo.Location = HitPair.Value->GetComponentLocation();
			BoxInfo.Rotation = HitPair.Value->GetComponentRotation();
			BoxInfo.BoxExtent = HitPair.Value->GetScaledBoxExtent();
			OutFramePackage.HitBoxInfo.Add(HitPair.Key, BoxInfo);
		}
	}
}

void ULafCompensasionComponent::ResetHitBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package)
{
	if (HitCharacter == nullptr) return;

	for (auto& HitPair : HitCharacter->HitCollisionBoxes)
	{
		if (HitPair.Value != nullptr)
		{
			HitPair.Value->SetWorldLocation(Package.HitBoxInfo[HitPair.Key].Location);
			HitPair.Value->SetWorldRotation(Package.HitBoxInfo[HitPair.Key].Rotation);
			HitPair.Value->SetBoxExtent(Package.HitBoxInfo[HitPair.Key].BoxExtent);
			HitPair.Value->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}
	}
}

void ULafCompensasionComponent::ENableCharacterMeshCollision(ABlasterCharacter* HitCharacter, ECollisionEnabled::Type CollisionType)
{
	if (HitCharacter && HitCharacter->GetMesh())
	{
		HitCharacter->GetMesh()->SetCollisionEnabled(CollisionType);
	}
}


void ULafCompensasionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	SaveFramePackageTick();
}


void ULafCompensasionComponent::SaveFramePackageTick()
{
	if (BlasterCharacter == nullptr || !BlasterCharacter->HasAuthority()) return;


	if (FrameHistory.Num() <= 1)//помещаем в лист начальное положение боксов
	{
		FFramePackage ThisFrame;
		SaveFramePackage(ThisFrame);
		FrameHistory.AddHead(ThisFrame);
	}
	else
	{
		float HistoryLengs = FrameHistory.GetHead()->GetValue().Time - FrameHistory.GetTail()->GetValue().Time;//вычисляем дельту между первым и последним в списке
		while (HistoryLengs > MaxFrameRecordHistory)//если это время больше чем указаное время обновления то цикл удаляет первый элемнт списка и будет делать это до тех пор пока условие не станет ложным
		{
			FrameHistory.RemoveNode(FrameHistory.GetTail());
			HistoryLengs = FrameHistory.GetHead()->GetValue().Time - FrameHistory.GetTail()->GetValue().Time;
		}
		FFramePackage ThisFrame;//далее сохраняем новый список с боксами
		SaveFramePackage(ThisFrame);
		FrameHistory.AddHead(ThisFrame);
	}
}


FServerSideRewindResult ULafCompensasionComponent::ProjectileServerSideRewind(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime)
{
	FFramePackage FrameToCheck = GetFrameToCheck(HitCharacter, HitTime);

	return ProjectileConfirmHit(FrameToCheck,HitCharacter, TraceStart, InitialVelocityProjectile, HitTime);
}



FServerSideRewindResult ULafCompensasionComponent::ProjectileConfirmHit(const FFramePackage& Package,ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime)
{
	FFramePackage CurrentFrame;

	CacheBoxPosition(HitCharacter, CurrentFrame);
	MoveBoxes(HitCharacter, CurrentFrame);
	ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::NoCollision);

	//Включить коллизию для головного бокса чтобы проверить попали ли мы в голову
	UBoxComponent* HeadBox = HitCharacter->HitCollisionBoxes[FName("head")];
	HeadBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	HeadBox->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);

	FPredictProjectilePathParams ProjectilePatchParam;
	ProjectilePatchParam.bTraceWithCollision = true;
	ProjectilePatchParam.MaxSimTime = MaxFrameRecordHistory;
	ProjectilePatchParam.LaunchVelocity = InitialVelocityProjectile;
	ProjectilePatchParam.StartLocation = TraceStart;
	ProjectilePatchParam.SimFrequency = 15.f;
	ProjectilePatchParam.ProjectileRadius = 5.f;
	ProjectilePatchParam.TraceChannel = ECC_HitBox;
	ProjectilePatchParam.ActorsToIgnore.Add(GetOwner());
	FPredictProjectilePathResult ProjectilePatchResult;
	UGameplayStatics::PredictProjectilePath(this, ProjectilePatchParam, ProjectilePatchResult);

	if (ProjectilePatchResult.HitResult.bBlockingHit) // we hit the head, return early
	{
		ResetHitBoxes(HitCharacter, CurrentFrame);
		ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::QueryAndPhysics);
		return FServerSideRewindResult{ true, true };
	}
	else // we didn't hit the head; check the rest of the boxes
	{
		for (auto& HitBoxPair : HitCharacter->HitCollisionBoxes)
		{
			if (HitBoxPair.Value != nullptr)
			{
				HitBoxPair.Value->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
				HitBoxPair.Value->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);
			}
		}

		UGameplayStatics::PredictProjectilePath(this, ProjectilePatchParam, ProjectilePatchResult);
		if (ProjectilePatchResult.HitResult.bBlockingHit)
		{
			ResetHitBoxes(HitCharacter, CurrentFrame);
			ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::QueryAndPhysics);
			return FServerSideRewindResult{ true, false };
		}
	}

	ResetHitBoxes(HitCharacter, CurrentFrame);
	ENableCharacterMeshCollision(HitCharacter, ECollisionEnabled::QueryAndPhysics);
	return FServerSideRewindResult{ false, false };
}



FServerSideRewindResult ULafCompensasionComponent::ServerSideRewind(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize& HitLocation, float HitTime)
{
	FFramePackage FrameToCheck = GetFrameToCheck(HitCharacter, HitTime);

	return ConfirmHit(FrameToCheck, HitCharacter, TraceStart, HitLocation);

}

FFramePackage ULafCompensasionComponent::InterpBeetwenFrames(const FFramePackage& OlderFrame, const FFramePackage& YoungerFrame, float HitFrame)
{
	const float Distance = YoungerFrame.Time - OlderFrame.Time;
	const float InterpFraction =FMath::Clamp((HitFrame - OlderFrame.Time) / Distance,0.f,1.f);

	FFramePackage InterpFramePackage;
	InterpFramePackage.Time = HitFrame;

	for (auto& YoungerPair : YoungerFrame.HitBoxInfo)
	{
		const FName& BoxInfoName = YoungerPair.Key;

		const FBoxInformation& OlderBox = OlderFrame.HitBoxInfo[BoxInfoName];
		const FBoxInformation& YoungerBox = YoungerFrame.HitBoxInfo[BoxInfoName];

		FBoxInformation InterpBoxInfo;

		InterpBoxInfo.Location = FMath::VInterpTo(OlderBox.Location, YoungerBox.Location, 1.f, InterpFraction);
		InterpBoxInfo.Rotation = FMath::RInterpTo(OlderBox.Rotation, YoungerBox.Rotation, 1.f, InterpFraction);
		InterpBoxInfo.BoxExtent = YoungerBox.BoxExtent;

		InterpFramePackage.HitBoxInfo.Add(BoxInfoName, InterpBoxInfo);
	}

	return InterpFramePackage;
}

void ULafCompensasionComponent::ServerScoreRequest_Implementation(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize& HitLocation, float HitTime)
{
	FServerSideRewindResult Confirm = ServerSideRewind(HitCharacter, TraceStart, HitLocation, HitTime);

	if (BlasterCharacter && BlasterCharacter->Controller && HitCharacter && BlasterCharacter->GetEquippedWeapon() && Confirm.bHitCinfirmed)
	{
		const float Damage = Confirm.bHeadShot ? BlasterCharacter->GetEquippedWeapon()->GetHeadShotDamageWeapon() : BlasterCharacter->GetEquippedWeapon()->GetDamageWeapon();
		UGameplayStatics::ApplyDamage(HitCharacter, Damage, BlasterCharacter->Controller, BlasterCharacter->GetEquippedWeapon(), UDamageType::StaticClass());
	}
}


void ULafCompensasionComponent::ProjectileServerScoreRequest_Implementation(ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime)
{
	FServerSideRewindResult Confirm = ProjectileServerSideRewind(HitCharacter, TraceStart, InitialVelocityProjectile, HitTime);

	if (BlasterCharacter && BlasterCharacter->Controller && HitCharacter  && Confirm.bHitCinfirmed && BlasterCharacter->GetEquippedWeapon())
	{
		const float Damage = Confirm.bHeadShot ? BlasterCharacter->GetEquippedWeapon()->GetHeadShotDamageWeapon() : BlasterCharacter->GetEquippedWeapon()->GetDamageWeapon();
		UGameplayStatics::ApplyDamage(HitCharacter, Damage, BlasterCharacter->Controller, BlasterCharacter->GetEquippedWeapon(), UDamageType::StaticClass());
	}
}


void ULafCompensasionComponent::ShotGunServerScoreRequest_Implementation(const TArray<ABlasterCharacter*>& HitCharacters, const FVector_NetQuantize& TraceStart, const TArray<FVector_NetQuantize>& HitLocations, float HitTime)
{
	FShotGunServerSideRewindResult Confirm = ShotgunServerSideRewind(HitCharacters, TraceStart, HitLocations, HitTime);

	for (auto& HitCharacter : HitCharacters)
	{
		if(HitCharacter == nullptr || HitCharacter->GetEquippedWeapon() == nullptr || BlasterCharacter == nullptr) continue;
		float TotalDamage{};
		if (Confirm.HeadShots.Contains(HitCharacter))
		{
			float HeadShotDamage = Confirm.HeadShots[HitCharacter] * HitCharacter->GetEquippedWeapon()->GetHeadShotDamageWeapon();
			TotalDamage += HeadShotDamage;
		}
		if (Confirm.BodyShots.Contains(HitCharacter))
		{
			float BodyShotDamage = Confirm.BodyShots[HitCharacter] * HitCharacter->GetEquippedWeapon()->GetDamageWeapon();
			TotalDamage += BodyShotDamage;
		}
		UGameplayStatics::ApplyDamage(HitCharacter, TotalDamage, BlasterCharacter->Controller, HitCharacter->GetEquippedWeapon(), UDamageType::StaticClass());
	}
}




FFramePackage ULafCompensasionComponent::GetFrameToCheck(ABlasterCharacter* HitCharacter, float HitTime)
{
	bool bReturn = HitCharacter == nullptr || HitCharacter->GetLagComponent() == nullptr || HitCharacter->GetLagComponent()->FrameHistory.GetHead() == nullptr || HitCharacter->GetLagComponent()->FrameHistory.GetTail() == nullptr;
	if (bReturn) return FFramePackage();
	//Локальная переменная для сервера пакета кадров для потверждения попадания
	FFramePackage FrameToCheck;

	bool bShouldInterpolate = true;

	//История фреймов для попадания в персонажа
	const TDoubleLinkedList<FFramePackage>& History = HitCharacter->GetLagComponent()->FrameHistory;
	const float OldestHistoryTime = History.GetTail()->GetValue().Time;
	const float NewestHistoryTime = History.GetHead()->GetValue().Time;
	if (OldestHistoryTime > HitTime)// Если время первого в списке больше чем время попадания то выходим из функции и не будем перематывать на стороне сервера, значит очень большая задержка клиента
	{
		return FFramePackage();
	}
	if (OldestHistoryTime == HitTime)// Если первый в листе время равно времени попаданию то скорей всего это сам сервер
	{
		FrameToCheck = History.GetTail()->GetValue();
		bShouldInterpolate = false;
	}
	if (NewestHistoryTime <= HitTime)// Если последние в списке листа меньше или равно времени попаданию то нужно проверить структуру попадания
	{
		FrameToCheck = History.GetHead()->GetValue();
		bShouldInterpolate = false;
	}

	TDoubleLinkedList<FFramePackage>::TDoubleLinkedListNode* Younger = History.GetHead();
	TDoubleLinkedList<FFramePackage>::TDoubleLinkedListNode* Older = Younger;

	while (Younger->GetValue().Time > HitTime)// Больше чем время хита, но еще моложе
	{
		//Марширование до тех пор пока более страое вермя не станет меньше чем время попадания
		if (Older->GetNextNode() == nullptr) break;
		Older = Older->GetNextNode();
		if (Older->GetValue().Time > HitTime)
		{
			Younger = Older;
		}
	}
	if (Older->GetValue().Time == HitTime)// это мало вероятно, но если это так то мы нашли кадр который не нужно проверять
	{
		FrameToCheck = Older->GetValue();
		bShouldInterpolate = false;
	}

	if (bShouldInterpolate)
	{
		FrameToCheck = InterpBeetwenFrames(Older->GetValue(), Younger->GetValue(), HitTime);
	}
	FrameToCheck.Character = HitCharacter;
	return FrameToCheck;
}

//ShotGun

FShotGunServerSideRewindResult ULafCompensasionComponent::ShotgunServerSideRewind(const TArray<ABlasterCharacter*>& HitCharacters, const FVector_NetQuantize& TraceStart, const TArray<FVector_NetQuantize>& HitLocations, float HitTime)
{
	TArray<FFramePackage> FramesToCheck;
	for (ABlasterCharacter* HitCharacter : HitCharacters)
	{
		FramesToCheck.Add(GetFrameToCheck(HitCharacter, HitTime));
	}
	return ShotgunConfirmHit(FramesToCheck, TraceStart, HitLocations);
}

FShotGunServerSideRewindResult ULafCompensasionComponent::ShotgunConfirmHit(const TArray<FFramePackage>& FramePackages, const FVector_NetQuantize& TraceStart, const TArray<FVector_NetQuantize>& HitLocations)
{
	FShotGunServerSideRewindResult ShotGunResult;

	for (auto& Frame : FramePackages)
	{
		if (Frame.Character == nullptr) return FShotGunServerSideRewindResult();
 	}

	TArray<FFramePackage>CurrentFrames;
	for (auto& Frame : FramePackages)
	{
		FFramePackage CurrentFrame;
		CurrentFrame.Character = Frame.Character;
		CacheBoxPosition(Frame.Character, CurrentFrame);
		MoveBoxes(Frame.Character, Frame);
		ENableCharacterMeshCollision(Frame.Character, ECollisionEnabled::NoCollision);
		CurrentFrames.Add(CurrentFrame);
	}

	for (auto& Frame: FramePackages)
	{
		//Включить коллизию для головного бокса чтобы проверить попали ли мы в голову
		UBoxComponent* HeadBox = Frame.Character->HitCollisionBoxes[FName("head")];
		HeadBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		HeadBox->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);
	}
	if (!GetWorld()) return FShotGunServerSideRewindResult();

	//check HeadShots ShotGun Server
	for (auto& HitLocation : HitLocations)
	{
		FHitResult HitResult;
		const FVector EndTrace = TraceStart + (HitLocation - TraceStart) * 1.25f;
		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, EndTrace, ECC_HitBox);
		ABlasterCharacter* BCharacter = Cast<ABlasterCharacter>(HitResult.GetActor());
		if (BCharacter)
		{
			if (ShotGunResult.HeadShots.Contains(BCharacter))
			{
				ShotGunResult.HeadShots[BCharacter]++;
			}
			else
			{
				ShotGunResult.HeadShots.Emplace(BCharacter,1);
			}
		}
	}
	//Disable Collision Head Box and Enabled Collision Body

	for (auto& Frame : FramePackages)
	{
		for (auto& HitBoxPair : Frame.Character->HitCollisionBoxes)
		{
			if (HitBoxPair.Value != nullptr)
			{
				HitBoxPair.Value->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
				HitBoxPair.Value->SetCollisionResponseToChannel(ECC_HitBox, ECollisionResponse::ECR_Block);
			}
		}
		UBoxComponent* HeadBox = Frame.Character->HitCollisionBoxes[FName("head")];
		HeadBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	// check Body shot ShotGun Server

	for (auto& HitLocation : HitLocations)
	{
		FHitResult HitResult;
		const FVector EndTrace = TraceStart + (HitLocation - TraceStart) * 1.25f;
		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, EndTrace, ECC_HitBox);
		ABlasterCharacter* BCharacter = Cast<ABlasterCharacter>(HitResult.GetActor());
		if (BCharacter)
		{
			if (ShotGunResult.BodyShots.Contains(BCharacter))
			{
				ShotGunResult.BodyShots[BCharacter]++;
			}
			else
			{
				ShotGunResult.BodyShots.Emplace(BCharacter, 1);
			}
		}
	}

	for (auto& Frame : CurrentFrames)
	{
		ResetHitBoxes(Frame.Character, Frame);
		ENableCharacterMeshCollision(Frame.Character, ECollisionEnabled::NoCollision);
	}
	
	return ShotGunResult;
}
