// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffComponent.h"
#include "OnlineBlasterCPP/Components/BuffComponent.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

UBuffComponent::UBuffComponent()
{
	
	PrimaryComponentTick.bCanEverTick = true;

	
}



void UBuffComponent::BeginPlay()
{
	Super::BeginPlay();

	
	
}

void UBuffComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	HealRump(DeltaTime);
	ShieldRump(DeltaTime);
}

//Health

void UBuffComponent::Heal(float HealAmount, float HealingTime)
{
	bHealing = true;
	HealingRate = HealAmount / HealingTime;
	AmountToHeal += HealAmount;
}



void UBuffComponent::HealRump(float DeltaTime)
{
	if (!bHealing || BlasterCharacter == nullptr ||BlasterCharacter->GetElim()) return;

	const float HealThisFrame = HealingRate * DeltaTime;

	BlasterCharacter->SetHEalth(FMath::Clamp(BlasterCharacter->GetHealth() + HealThisFrame, 0, BlasterCharacter->GetMaxHealth()));
	AmountToHeal -= HealThisFrame;
	BlasterCharacter->UpdateHUDHealth();
	

	if (AmountToHeal <= 0.f || BlasterCharacter->GetHealth() >= BlasterCharacter->GetMaxHealth())
	{
		bHealing = false;
		AmountToHeal = 0.f;
	}
}



//Speed


void UBuffComponent::MulticastSetSpeed_Implementation(float StandSpeed, float CrouchSpeed)
{
	BlasterCharacter->GetCharacterMovement()->MaxWalkSpeed = StandSpeed;
	BlasterCharacter->GetCharacterMovement()->MaxWalkSpeedCrouched = CrouchSpeed;
}

void UBuffComponent::BuffSpeed(float StandBuffSpeed, float BuffCrouchSpeed, float TimeBuff)
{
	if (BlasterCharacter == nullptr) return;

	BlasterCharacter->GetWorldTimerManager().SetTimer(SpeedHandleTimer, this, &UBuffComponent::ResetSpeed,TimeBuff);

	if (BlasterCharacter->GetCharacterMovement())
	{
		MulticastSetSpeed(StandBuffSpeed, BuffCrouchSpeed);
	}
}

void UBuffComponent::InitialSpeed(float StandSpeed, float CrouchSpeed)
{
	InitialiStandSpeed = StandSpeed;
	InitialCrouchSpeed = CrouchSpeed;
}

void UBuffComponent::ResetSpeed()
{
	if (BlasterCharacter == nullptr || BlasterCharacter->GetCharacterMovement() == nullptr) return;
	
	MulticastSetSpeed(InitialiStandSpeed,InitialCrouchSpeed);
}



//Jump

void UBuffComponent::InitializeJumpZ(float ZJumpvelocity)
{
	InitializeJumpZVelocity = ZJumpvelocity;
}

void UBuffComponent::ResetBuffJump()
{
	if (BlasterCharacter == nullptr || BlasterCharacter->GetCharacterMovement() == nullptr) return;
	MulticastJumpZVelocity(InitializeJumpZVelocity);

}

void UBuffComponent::MulticastJumpZVelocity_Implementation(float Velocity)
{
	BlasterCharacter->GetCharacterMovement()->JumpZVelocity = Velocity;
}

void UBuffComponent::BuffJump(float ZVelocity, float TimeBuff)
{
	if (BlasterCharacter == nullptr) return;

	BlasterCharacter->GetWorldTimerManager().SetTimer(JumpTimerHandle, this, &UBuffComponent::ResetBuffJump, TimeBuff);

	if (BlasterCharacter->GetCharacterMovement())
	{
		MulticastJumpZVelocity(ZVelocity);
	}
}

//Shield

void UBuffComponent::Shield(float ShieldAmount, float ShieldTime)
{
	bShieldHealing = true;
	ShieldRate = ShieldAmount / ShieldTime;
	AmountToShield += ShieldAmount;
}

void UBuffComponent::ShieldRump(float DeltaTime)
{
	if (!bShieldHealing || BlasterCharacter == nullptr || BlasterCharacter->GetElim()) return;

	const float ShieldThisFrame = ShieldRate * DeltaTime;

	BlasterCharacter->SetShield(FMath::Clamp(BlasterCharacter->GetShield() + ShieldThisFrame, 0, BlasterCharacter->GetMaxShield()));
	AmountToShield -= ShieldThisFrame;
	BlasterCharacter->UpdateHUDShield();


	if (AmountToShield <= 0.f || BlasterCharacter->GetShield() >= BlasterCharacter->GetMaxShield())
	{
		bShieldHealing = false;
		AmountToShield = 0.f;
	}
}