// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OnlineBlasterCPP/HUD/BlasterHUD.h"
#include "OnlineBlasterCPP/TurningInPlace.h"
#include "CombatComponent.generated.h"

#define  TRACE_LENGHT 80'000.f;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINEBLASTERCPP_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UCombatComponent();
	friend class ABlasterCharacter;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


	void EquipWeapon(class AWeapon* WeaponEquip);
	void SwapWeapon();

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void FireButtonPressed(bool bIsPressed);


	UFUNCTION(BlueprintCallable)
		void ShotGunShellReload();

	void TrowGrenade();

	UFUNCTION(Server,Reliable)
		void ServerThrowGrenade();

	UFUNCTION(BlueprintCallable)
		void TrowGrenadeFinish();

	bool bLocallyReload = false;

protected:
	
	virtual void BeginPlay() override;

		void SetAiming(bool bNewAiming);
	UFUNCTION(Server,Reliable)
		void ServerSetAiming(bool bNewAiming);


	void EquipPrimaryWeapon(AWeapon* WeaponToEquip);
	void EquipSecondaryWeapon(AWeapon* WeaponToEquip);


	//Fire
		void Fire();
		void LocalFire(const FVector_NetQuantize& TraceHitTarget);
		void LocalShotGunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);
		void FireProjectileWeapon();
		void FireHitScanWeapon();
		void FireShotGunWeapon();

	UFUNCTION(Server,Reliable,WithValidation)
		void ServerFire(const FVector_NetQuantize TraceHitTarget, float WeaponFireDelay);		
	UFUNCTION(NetMulticast, Reliable)
		void MulticastFire(const FVector_NetQuantize TraceHitTarget);

	UFUNCTION(Server, Reliable,WithValidation)
		void ServerShotGunFire(const TArray<FVector_NetQuantize>& TraceHitTargets, float WeaponFireDelay);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastShotGunFire(const TArray<FVector_NetQuantize>& TraceHitTargets);

		void TraceUnderCrossHair(FHitResult& TraceHitResult);

		void SetHUDCrossHair(float DeltaTime);

	UFUNCTION(Server, Reliable)
		void ServerReloadWeapon();

	void DropEquipWeapon();
	void AttachActorRightHand(AActor* AttachActor);
	void AttachActorLeftHand(AActor* AttachActor);
	void AttachActorToBackPack(AActor* AttachActor);
	void UpdateCurridAmmo();
	void PlayEquipWeaponSound(AWeapon* WeaponToEquip);
	void ReloaEmpthyWeapon();
	void ShowAttachGrenadeMesh(bool bShowMesh);
	UFUNCTION(BlueprintCallable)
		void LauchGrenade();
	UFUNCTION(Server,Reliable)
		void ServerLaunchGrenade(const FVector_NetQuantize& TargetHit);

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AProjectile> GrenadeClass;

private:
	UPROPERTY()
	class ABlasterCharacter* Character;
	UPROPERTY()
	class ABlasterPlayerController* Controller;
	UPROPERTY()
	class ABlasterHUD* HUD;

	UPROPERTY(ReplicatedUsing = OnRep_EquppedWeapon)
		 AWeapon* EquippedWeapon;

	UFUNCTION()
		void OnRep_EquppedWeapon();

	UPROPERTY(ReplicatedUsing = OnRep_SecondaryWeapon)
		AWeapon* SecondaryWeapon;

	UFUNCTION()
		void OnRep_SecondaryWeapon();

	UPROPERTY(ReplicatedUsing = OnRep_bAiming)
		bool bAiming = false;

	bool bAimingButtonPressed = false;

	UFUNCTION()
		void OnRep_bAiming();

	float BaseWalkSpeed;
	float AimWalkSpeed;

	bool bFireButtonPressed;

	float CrossHairVelocityFactor;
	float CrossHaitAirFactor;
	float CrossHairAimFactor;
	float CrossHairShootingFactor;

	FVector HitTarget;

	float DefaultFOV;
	float CurrentFOV;

	UPROPERTY(EditAnywhere, Category = "Combat")
		float ZoomedFOV = 30.f;
	UPROPERTY(EditAnywhere, Category = "Combat")
		float ZoomInterpSpeed = 20.f;

	FHUDPackage HudPackage;

	void InterpFOV(float DeltaTime);

	//FireWeaponTimer

	FTimerHandle FireTimerHandle;

	bool bCanFire = true;

	void StartFireTimer();
	void FinishFireTimer();


	bool CanFire();



	//Carried Ammo
	

	UPROPERTY(ReplicatedUsing = OnRep_CurriedAmmo)
		int32 CurriedAmmo;

	UFUNCTION()
		void OnRep_CurriedAmmo();

	TMap<EWeaponType,int32> CarriedAmmoType;


	UPROPERTY(EditAnywhere)
		int32 StartingARAmmo = 30;
	UPROPERTY(EditAnywhere)
		int32 StartingRocketAmmo = 0;
	UPROPERTY(EditAnywhere)
		int32 StartingPistolAmmo = 15;
	UPROPERTY(EditAnywhere)
		int32 StartingSMGAmmo = 25;
	UPROPERTY(EditAnywhere)
		int32 StartingShotGunAmmo = 10;
	UPROPERTY(EditAnywhere)
		int32 StartingSniperRirleAmmo = 7;
	UPROPERTY(EditAnywhere)
		int32 StartingGrenadeLauncherAmmo = 3;

	void InitializeCurriedAmmo();

	//Reload

	UPROPERTY(ReplicatedUsing = OnRep_CombatState)
	ECombatState CombatState = ECombatState::ECS_Unoccupied;

	UFUNCTION()
		void OnRep_CombatState();

	void HandleReloadWeapon();
	int32 AmountToReload();
	UFUNCTION(BlueprintCallable)
		void FinishReloading();

	UFUNCTION(BlueprintCallable)
		void FinishSwapWeapon();

	UFUNCTION(BlueprintCallable)
		void FinishSwapAttachWeapon();


	//Grenade
	UPROPERTY(ReplicatedUsing = OnRep_Grenades)
	int32 Grenades = 3;

	UFUNCTION()
		void OnRep_Grenades();

	UPROPERTY(EditAnywhere)
		int32 MaxGrenades = 4;

	void UpdateHUDGrenades();

public:	
	
	void ReloadWeapon();

	

	void JumpToShotGunEnd();

	UFUNCTION(BlueprintCallable)
		void UpdateShotGunAmmoValues();


	FORCEINLINE int32 GetGrenadeCount() const { return Grenades; }


	void PickUPAmmo(EWeaponType AmmoType, int32 AmmoAmount);

	//SwapWeapon

	bool ShouldSwapWeapons();
};
