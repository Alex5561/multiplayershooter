// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BuffComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINEBLASTERCPP_API UBuffComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	friend class ABlasterCharacter;

	UBuffComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void Heal(float HealAmount, float HealingTime);
	void Shield(float ShieldAmount, float ShieldTime);
	void BuffSpeed(float StandBuffSpeed, float BuffCrouchSpeed, float TimeBuff);
	void InitialSpeed(float StandSpeed, float CrouchSpeed);
	void InitializeJumpZ(float ZJumpvelocity);
	void BuffJump(float ZVelocity, float TimeBuff);

protected:
	
	virtual void BeginPlay() override;

	void HealRump(float DeltaTime);

	void ShieldRump(float DeltaTime);

	UFUNCTION(NetMulticast,Reliable)
		void MulticastSetSpeed(float StandSpeed, float CrouchSpeed);

private:

	UPROPERTY()
		class ABlasterCharacter* BlasterCharacter;
	

	//Health Buff
	bool bHealing = false;
	float HealingRate = 0.f;
	float AmountToHeal = 0.f;

	//Speed Buff
	
	FTimerHandle SpeedHandleTimer;
	void ResetSpeed();

	float InitialiStandSpeed;
	float InitialCrouchSpeed;


	//Jump

	FTimerHandle JumpTimerHandle;

	void ResetBuffJump();

	float InitializeJumpZVelocity;
		
	UFUNCTION(NetMulticast,Reliable)
		void MulticastJumpZVelocity(float Velocity);

	//Shield Buff

	bool bShieldHealing = false;
	float ShieldRate = 0.f;
	float AmountToShield = 0.f;
	
};
