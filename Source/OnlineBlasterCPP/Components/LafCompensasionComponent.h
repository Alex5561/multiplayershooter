// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LafCompensasionComponent.generated.h"



USTRUCT(BlueprintType)
struct FBoxInformation
{
	GENERATED_BODY()

	UPROPERTY()
		FVector Location;

	UPROPERTY()
		FRotator Rotation;

	UPROPERTY()
		FVector BoxExtent;
};

USTRUCT(BlueprintType)
struct FFramePackage
{
	GENERATED_BODY()

	UPROPERTY()
		float Time;

	UPROPERTY()
		TMap<FName, FBoxInformation> HitBoxInfo;

	UPROPERTY()
		ABlasterCharacter* Character;
};


USTRUCT(BlueprintType)
struct FServerSideRewindResult
{
	GENERATED_BODY()

	UPROPERTY()
		bool bHitCinfirmed;

	UPROPERTY()
		bool bHeadShot;
};

USTRUCT(BlueprintType)
struct FShotGunServerSideRewindResult
{
	GENERATED_BODY()

	UPROPERTY()
		TMap<ABlasterCharacter*,uint32> HeadShots;
	UPROPERTY()
		TMap<ABlasterCharacter*, uint32> BodyShots;
};

/****************************************************************************************************************************************************************************/

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ONLINEBLASTERCPP_API ULafCompensasionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	ULafCompensasionComponent();
	friend class ABlasterCharacter;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	

	// Перемотка со стороны сервера

	//HitScan
	FServerSideRewindResult ServerSideRewind(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize& HitLocation, float HitTime);

	FFramePackage InterpBeetwenFrames(const FFramePackage& OlderFrame, const FFramePackage& YoungerFrame, float HitFrame);

	//Hitscan
	UFUNCTION(Server,Reliable)
		void ServerScoreRequest(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize& HitLocation, float HitTime);
	//shotgun
	UFUNCTION(Server, Reliable)
		void ShotGunServerScoreRequest(const TArray<ABlasterCharacter*>& HitCharacters, const FVector_NetQuantize& TraceStart, const TArray<FVector_NetQuantize>& HitLocations, float HitTime);

	//ProjectileWeapon
	FServerSideRewindResult ProjectileServerSideRewind(class ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime);
	FServerSideRewindResult ProjectileConfirmHit(const FFramePackage& Package,ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime);
	UFUNCTION(Server, Reliable)
		void ProjectileServerScoreRequest(ABlasterCharacter* HitCharacter, const FVector_NetQuantize& TraceStart, const FVector_NetQuantize100& InitialVelocityProjectile, float HitTime);
protected:
	
	virtual void BeginPlay() override;
	void SaveFramePackage(FFramePackage& Package);


	FServerSideRewindResult ConfirmHit(const FFramePackage& Package, ABlasterCharacter* HitCharacter, const FVector_NetQuantize TraceStart, const FVector_NetQuantize& HitLocation);
	void MoveBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package);
	void CacheBoxPosition(ABlasterCharacter* HitCharacter, FFramePackage& OutFramePackage);
	void ResetHitBoxes(ABlasterCharacter* HitCharacter, const FFramePackage& Package);
	void ENableCharacterMeshCollision(ABlasterCharacter* HitCharacter, ECollisionEnabled::Type CollisionType);
	void SaveFramePackageTick();
	FFramePackage GetFrameToCheck(ABlasterCharacter* HitCharacter, float HitTime);

	//ShotGun

	FShotGunServerSideRewindResult ShotgunServerSideRewind(const TArray<ABlasterCharacter*>& HitCharacters,const FVector_NetQuantize& TraceStart,const TArray<FVector_NetQuantize>& HitLocations,float HitTime);
	FShotGunServerSideRewindResult ShotgunConfirmHit(const TArray<FFramePackage>& FramePackages,const FVector_NetQuantize& TraceStart,const TArray<FVector_NetQuantize>& HitLocations);
private:

	UPROPERTY()
		class ABlasterCharacter* BlasterCharacter;
	UPROPERTY()
		class ABlasterPlayerController* BlasterPlayerController;

	//Double List Hidtory Box Character
	TDoubleLinkedList<FFramePackage> FrameHistory;

	UPROPERTY(EditAnywhere)
		float MaxFrameRecordHistory = 4.f;

		
};
