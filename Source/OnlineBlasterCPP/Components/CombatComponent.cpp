// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatComponent.h"
#include "OnlineBlasterCPP/Weapon/Weapon.h"
#include "OnlineBlasterCPP/Character/BlasterCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "OnlineBlasterCPP/PlayerController/BlasterPlayerController.h"
#include "OnlineBlasterCPP/HUD/BlasterHUD.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "OnlineBlasterCPP/Character/BlasterAnimInstance.h"
#include "OnlineBlasterCPP/Weapon/Projectile/Projectile.h"
#include "OnlineBlasterCPP/Weapon/ShotGunWeapon.h"


UCombatComponent::UCombatComponent() 
{
	
	PrimaryComponentTick.bCanEverTick = true;

	BaseWalkSpeed = 600.f;
	AimWalkSpeed = 450.f;
}

void UCombatComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCombatComponent,EquippedWeapon);
	DOREPLIFETIME(UCombatComponent, SecondaryWeapon);
	DOREPLIFETIME(UCombatComponent, bAiming);
	DOREPLIFETIME_CONDITION(UCombatComponent, CurriedAmmo,COND_OwnerOnly);
	DOREPLIFETIME(UCombatComponent,CombatState);
	DOREPLIFETIME(UCombatComponent, Grenades);
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
		if (Character->GetFollowCamera())
		{
			DefaultFOV = Character->GetFollowCamera()->FieldOfView;
			CurrentFOV = DefaultFOV;
		}
		if (Character->HasAuthority())
		{
			InitializeCurriedAmmo();
		}
	}

}

void UCombatComponent::SetAiming(bool bNewAiming)
{
	if (Character == nullptr || EquippedWeapon == nullptr) return;
	bAiming = bNewAiming;
	ServerSetAiming(bNewAiming);
	if (Character)
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = bNewAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
	if (Character->IsLocallyControlled()  && EquippedWeapon->GetWeaponType() == EWeaponType::EWT_SniperRifle)
	{
		Character->ShowSniperScopeWidget(bAiming);
	}
	if(Character->IsLocallyControlled()) bAimingButtonPressed = bNewAiming;
}

void UCombatComponent::ServerSetAiming_Implementation(bool bNewAiming)
{
	bAiming = bNewAiming;
	{
		Character->GetCharacterMovement()->MaxWalkSpeed = bNewAiming ? AimWalkSpeed : BaseWalkSpeed;
	}
}

void UCombatComponent::OnRep_bAiming()
{
	if (Character && Character->IsLocallyControlled())
	{
		bAiming = bAimingButtonPressed;
	}
}

void UCombatComponent::OnRep_EquppedWeapon()
{
	if (EquippedWeapon && Character)
	{
		EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equpped);
		AttachActorRightHand(EquippedWeapon);
		PlayEquipWeaponSound(EquippedWeapon);
		Character->GetCharacterMovement()->bOrientRotationToMovement = false;
		Character->bUseControllerRotationYaw = true;
		EquippedWeapon->SetHUDAmmo();
	}
}


void UCombatComponent::OnRep_SecondaryWeapon()
{
	if (SecondaryWeapon && Character)
	{
		SecondaryWeapon->SetWeaponState(EWeaponState::EWS_Equpped);
		AttachActorToBackPack(SecondaryWeapon);
		PlayEquipWeaponSound(SecondaryWeapon);
	}
}



void UCombatComponent::FireButtonPressed(bool bIsPressed)
{
	bFireButtonPressed = bIsPressed;
	if (bFireButtonPressed)
	{
		if(EquippedWeapon == nullptr) return;
		Fire();
	}
}



void UCombatComponent::Fire()
{
	if (CanFire())
	{
		
		bCanFire = false;
		
		if (EquippedWeapon)
		{
			CrossHairShootingFactor = 0.75f;
			switch (EquippedWeapon->FireTypeWeapon)
			{
			case EFireType::EFT_HitScanWeapon :
				FireHitScanWeapon();
				break;
			case EFireType::EFT_ProjectileWeapon :
				FireProjectileWeapon();
				break;
			case EFireType::EFT_ShotGunWeapon :
				FireShotGunWeapon();
				break;
			}
		}
		StartFireTimer();
	}
}

void UCombatComponent::LocalFire(const FVector_NetQuantize& TraceHitTarget)
{
	if (EquippedWeapon == nullptr) return;

	if (Character && CombatState == ECombatState::ECS_Unoccupied)
	{
		Character->PlayFireMontage(bAiming);
		EquippedWeapon->Fire(TraceHitTarget);
	}
}

void UCombatComponent::LocalShotGunFire(const TArray<FVector_NetQuantize>& TraceHitTargets)
{
	AShotGunWeapon* ShotGun = Cast<AShotGunWeapon>(EquippedWeapon);
	if (Character == nullptr || ShotGun == nullptr) return;

	if (CombatState == ECombatState::ECS_Reloading || CombatState == ECombatState::ECS_Unoccupied)
	{
		Character->PlayFireMontage(bAiming);
		ShotGun->FireShotGun(TraceHitTargets);
		CombatState = ECombatState::ECS_Unoccupied;
	}


}

void UCombatComponent::FireProjectileWeapon()
{
	if (EquippedWeapon && Character)
	{
		if(!Character->HasAuthority()) LocalFire(HitTarget);
		ServerFire(HitTarget,EquippedWeapon->FireDelay);
	}
}

void UCombatComponent::FireHitScanWeapon()
{
	if (EquippedWeapon && Character)
	{
		HitTarget = EquippedWeapon->bUseScatter ? EquippedWeapon->TraceEndThisScatter(HitTarget) : HitTarget;
		if (!Character->HasAuthority()) LocalFire(HitTarget);
		ServerFire(HitTarget, EquippedWeapon->FireDelay);
	}
}

void UCombatComponent::FireShotGunWeapon()
{
	AShotGunWeapon* ShotGun = Cast<AShotGunWeapon>(EquippedWeapon);
	if (ShotGun && Character)
	{
		TArray<FVector_NetQuantize>HitTargets;
		ShotGun->ShotGunTraceEndHitScatter(HitTarget, HitTargets);
		if (!Character->HasAuthority()) LocalShotGunFire(HitTargets);
		ServerShotGunFire(HitTargets, EquippedWeapon->FireDelay);
	}
}

void UCombatComponent::ServerFire_Implementation(const FVector_NetQuantize TraceHitTarget, float WeaponFireDelay)
{
	MulticastFire(TraceHitTarget);
}

bool UCombatComponent::ServerFire_Validate(const FVector_NetQuantize TraceHitTarget, float WeaponFireDelay)
{
	if (EquippedWeapon)
	{
		bool bIsNearlyFireDelay = FMath::IsNearlyEqual(EquippedWeapon->FireDelay, WeaponFireDelay, 0.001f);
		return bIsNearlyFireDelay;
	}
	return true;
}

void UCombatComponent::MulticastFire_Implementation(const FVector_NetQuantize TraceHitTarget)
{
	if (Character && Character->IsLocallyControlled() && !Character->HasAuthority()) return;
	LocalFire(TraceHitTarget);

}

void UCombatComponent::ServerShotGunFire_Implementation(const TArray<FVector_NetQuantize>& TraceHitTargets, float WeaponFireDelay)
{
	MulticastShotGunFire(TraceHitTargets);
}

bool UCombatComponent::ServerShotGunFire_Validate(const TArray<FVector_NetQuantize>& TraceHitTargets, float WeaponFireDelay)
{
	if (EquippedWeapon)
	{
		bool bIsNearlyFireDelay = FMath::IsNearlyEqual(EquippedWeapon->FireDelay, WeaponFireDelay, 0.001f);
		return bIsNearlyFireDelay;
	}
	return true;
}

void UCombatComponent::MulticastShotGunFire_Implementation(const TArray<FVector_NetQuantize>& TraceHitTargets)
{
	if (Character && Character->IsLocallyControlled() && !Character->HasAuthority()) return;
	LocalShotGunFire(TraceHitTargets);


}

void UCombatComponent::TraceUnderCrossHair(FHitResult& TraceHitResult)
{
	if (EquippedWeapon)
	{
		FVector2D ViemportSize;
		if (GEngine && GEngine->GameViewport)
		{
			GEngine->GameViewport->GetViewportSize(ViemportSize);
		}
		FVector2D CrossHairLocation(ViemportSize.X / 2.f, ViemportSize.Y / 2.f);
		FVector WorldLocation;
		FVector WorldDirection;
		bool ScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(UGameplayStatics::GetPlayerController(this, 0), CrossHairLocation, WorldLocation, WorldDirection);
		if (ScreenToWorld)
		{
			FVector Start = WorldLocation;

			if (Character)
			{
				float DistanceToCharacter = (Character->GetActorLocation() - Start).Size();
				Start += WorldDirection * (DistanceToCharacter + 100.f);  
			}

			FVector End = Start + WorldDirection * TRACE_LENGHT;
			GetWorld()->LineTraceSingleByChannel(TraceHitResult, Start, End, ECollisionChannel::ECC_Visibility);
			if (TraceHitResult.bBlockingHit)
			{
				
				if (TraceHitResult.GetActor() && TraceHitResult.GetActor()->Implements<UInteractWithCrossHairInterface>())
				{
					HudPackage.CrossHairColor = FLinearColor::Red;
				}
				else
				{
					HudPackage.CrossHairColor = FLinearColor::White;
				}
			}
			else
			{
				TraceHitResult.ImpactPoint = End;
			}
		}
	}
	
}

void UCombatComponent::SetHUDCrossHair(float DeltaTime)
{
	if(Character == nullptr) return;

	Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->GetController()) : Controller;
	if (Controller)
	{
		HUD = HUD == nullptr ? Cast<ABlasterHUD>(Controller->GetHUD()) : HUD;
		if (HUD)
		{
	
			if (EquippedWeapon)
			{
				HudPackage.CrossHairCenter = EquippedWeapon->CrossHairCenter;
				HudPackage.CrossHairLeft = EquippedWeapon->CrossHairLeft;
				HudPackage.CrossHairRight = EquippedWeapon->CrossHairRight;
				HudPackage.CrossHairTop = EquippedWeapon->CrossHairTop;
				HudPackage.CrossHairButtom = EquippedWeapon->CrossHairBottom;
				HUD->SetHUDPackage(HudPackage);
			}
			else
			{
				HudPackage.CrossHairCenter = nullptr;
				HudPackage.CrossHairLeft = nullptr;
				HudPackage.CrossHairRight = nullptr;
				HudPackage.CrossHairTop = nullptr;
				HudPackage.CrossHairButtom = nullptr;
			}
			//Calculate CroosHair
				// [0,600] [0,1] Velocity

			FVector2D MaxWalkSpeedRange(0.f, Character->GetCharacterMovement()->MaxWalkSpeed);
			FVector2D VelocityMultiplayRange(0.f, 1.f);
			FVector Velocity = Character->GetVelocity();
			Velocity.Z = 0.f;
			CrossHairVelocityFactor = FMath::GetMappedRangeValueClamped(MaxWalkSpeedRange, VelocityMultiplayRange, Velocity.Size());



			if (Character->GetCharacterMovement()->IsFalling())
			{
				CrossHaitAirFactor = FMath::FInterpTo(CrossHaitAirFactor, 2.25f, DeltaTime, 2.25f);
			}
			else
			{
				CrossHaitAirFactor = FMath::FInterpTo(CrossHaitAirFactor, 0.f, DeltaTime, 2.25f);
			}

			if (bAiming)
			{
				CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor,0.58f,DeltaTime,30.f);
			}
			else
			{
				CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor, 0.f, DeltaTime, 30.f);
			}

			CrossHairShootingFactor = FMath::FInterpTo(CrossHairShootingFactor, 0.f, DeltaTime, 40.f);

			HudPackage.CrossHairSpread =0.5f + CrossHairVelocityFactor + CrossHaitAirFactor - CrossHairAimFactor + CrossHairShootingFactor;
			HUD->SetHUDPackage(HudPackage);
		}
	}
}



void UCombatComponent::InterpFOV(float DeltaTime)
{
	if(EquippedWeapon == nullptr) return;

	if (bAiming)
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV,EquippedWeapon->GetZoomedFOV(),DeltaTime,EquippedWeapon->GetZoomInterpSpeed());
	}
	else
	{
		CurrentFOV = FMath::FInterpTo(CurrentFOV, DefaultFOV, DeltaTime, ZoomInterpSpeed);
	}
	if (Character && Character->GetFollowCamera())
	{
		Character->GetFollowCamera()->SetFieldOfView(CurrentFOV);
	}
}

void UCombatComponent::StartFireTimer()
{
	if(EquippedWeapon == nullptr || Character == nullptr)  return;

	Character->GetWorldTimerManager().SetTimer(FireTimerHandle,this,&UCombatComponent::FinishFireTimer,EquippedWeapon->FireDelay);
}

void UCombatComponent::FinishFireTimer()
{
	if (EquippedWeapon == nullptr || Character == nullptr)  return;
	
	bCanFire = true;
	if (bFireButtonPressed && EquippedWeapon->bAutomatic)
	{
		Fire();
	}
	ReloaEmpthyWeapon();
}

bool UCombatComponent::CanFire()
{
	if(EquippedWeapon == nullptr) return false;
	if (!EquippedWeapon->IsEmpthy() && bCanFire && CombatState == ECombatState::ECS_Unoccupied && EquippedWeapon->GetWeaponType() == EWeaponType::EWT_ShotGun) return true;
	if (bLocallyReload) return false;
	return !EquippedWeapon->IsEmpthy() && bCanFire && CombatState==ECombatState::ECS_Unoccupied;
	
}

void UCombatComponent::OnRep_CurriedAmmo()
{
	Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->Controller) : Controller;
	if (Controller)
	{
		Controller->SetHUDCurriedAmmo(CurriedAmmo);
	}
	if (CombatState == ECombatState::ECS_Reloading && EquippedWeapon != nullptr && EquippedWeapon->GetWeaponType() == EWeaponType::EWT_ShotGun && CurriedAmmo == 0)
	{
		JumpToShotGunEnd();
	}
}

void UCombatComponent::InitializeCurriedAmmo()
{
	CarriedAmmoType.Emplace(EWeaponType::EWT_AssaultRifle, StartingARAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_RocketLauncher, StartingRocketAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_Pistol, StartingPistolAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_SMG, StartingSMGAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_ShotGun, StartingShotGunAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_SniperRifle, StartingSniperRirleAmmo);
	CarriedAmmoType.Emplace(EWeaponType::EWT_GrenadeLauncher, StartingGrenadeLauncherAmmo);
}


void UCombatComponent::OnRep_CombatState()
{
	switch (CombatState)
	{
	case ECombatState::ECS_Unoccupied:
		if (bFireButtonPressed)
		{
			Fire();
		}
		break;
	case ECombatState::ECS_Reloading:
		if(Character && !Character->IsLocallyControlled()) HandleReloadWeapon();
		break;
	case ECombatState::ECS_TrowhGrenade:
		if (Character && !Character->IsLocallyControlled())
		{
			Character->PlayThrowGrenadeMontage();
			AttachActorLeftHand(EquippedWeapon);
			ShowAttachGrenadeMesh(true);
		}
		break;
	case ECombatState::ECS_SwapWeapon :
		if (Character && !Character->IsLocallyControlled())
		{
			Character->PlaySwapWeaponMontage();
		}
		break;
	case ECombatState::ECS_DefaultMax:
		break;
	default:
		break;
	}
}

void UCombatComponent::HandleReloadWeapon()
{
	if (Character)
	{
		Character->PlayReloadWeaponMontage();
	}
}

int32 UCombatComponent::AmountToReload()
{
	if(EquippedWeapon == nullptr) return 0;
	int32 RoomAmmo = EquippedWeapon->GetMagCapacity() - EquippedWeapon->GetAmmo();
	if (CarriedAmmoType.Contains(EquippedWeapon->GetWeaponType()))
	{
		int32 CurriedBullet = CarriedAmmoType[EquippedWeapon->GetWeaponType()];
		int32 Least = FMath::Min(RoomAmmo,CurriedBullet);
		return FMath::Clamp(RoomAmmo,0,Least);
	}
	return 0;
}

void UCombatComponent::FinishReloading()
{	
	if(Character == nullptr) return;
	bLocallyReload = false;
	if (Character->HasAuthority())
	{
		/*CombatState = ECombatState::ECS_Unoccupied;*/

		int32 ReloadAmount = AmountToReload();
		if (CarriedAmmoType.Contains(EquippedWeapon->GetWeaponType()))
		{
			CarriedAmmoType[EquippedWeapon->GetWeaponType()] -= ReloadAmount;
			CurriedAmmo = CarriedAmmoType[EquippedWeapon->GetWeaponType()];
		}
		EquippedWeapon->AddAmmo(ReloadAmount);
		CombatState = ECombatState::ECS_Unoccupied;
		Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->Controller) : Controller;
		if (Controller)
		{
			Controller->SetHUDCurriedAmmo(CurriedAmmo);
		}
	}
	if (bFireButtonPressed)
	{
		Fire();
	}
}




void UCombatComponent::FinishSwapWeapon()
{
	if (Character && Character->HasAuthority())
	{
		CombatState = ECombatState::ECS_Unoccupied;
	}
	if (Character) Character->bFinishSwapping = true;

}

void UCombatComponent::FinishSwapAttachWeapon()
{
	
	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equpped);
	AttachActorRightHand(EquippedWeapon);
	EquippedWeapon->SetHUDAmmo();
	UpdateCurridAmmo();
	PlayEquipWeaponSound(EquippedWeapon);

	SecondaryWeapon->SetWeaponState(EWeaponState::EWS_EquppedSecondary);
	AttachActorToBackPack(SecondaryWeapon);
}

void UCombatComponent::UpdateShotGunAmmoValues()
{
	if (Character == nullptr || EquippedWeapon == nullptr) return;

	if (CarriedAmmoType.Contains(EquippedWeapon->GetWeaponType()))
	{
		CarriedAmmoType[EquippedWeapon->GetWeaponType()] -= 1;
		CurriedAmmo = CarriedAmmoType[EquippedWeapon->GetWeaponType()];
	}
	Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->Controller) : Controller;
	if (Controller)
	{
		Controller->SetHUDCurriedAmmo(CurriedAmmo);
	}
	EquippedWeapon->AddAmmo(1);
	bCanFire = true;
	if (EquippedWeapon->IsFullAmmo() || CurriedAmmo == 0)
	{
		//JumpEndMontageSection
		JumpToShotGunEnd();
	}
}

void UCombatComponent::PickUPAmmo(EWeaponType AmmoType, int32 AmmoAmount)
{
	if (CarriedAmmoType.Contains(AmmoType))
	{
		CarriedAmmoType[AmmoType] += AmmoAmount;

		UpdateCurridAmmo();
	}
	if (EquippedWeapon && EquippedWeapon->IsEmpthy() && EquippedWeapon->GetWeaponType() == AmmoType)
	{
		ReloadWeapon();
	}
}



void UCombatComponent::JumpToShotGunEnd()
{
	UAnimInstance* AnimInstance = Character->GetMesh()->GetAnimInstance();
	if (AnimInstance && Character->GetReloadMontage())
	{
		AnimInstance->Montage_JumpToSection(FName("ShotGunEnd"));
	}
}


void UCombatComponent::ShotGunShellReload()
{
	if (Character && Character->HasAuthority())
	{
		UpdateShotGunAmmoValues();
	}
	
}

void UCombatComponent::OnRep_Grenades()
{
	UpdateHUDGrenades();
}

void UCombatComponent::UpdateHUDGrenades()
{
	Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->Controller) : Controller;
	if (Controller)
	{
		Controller->SetHUDGrenade(Grenades);
	}
}

void UCombatComponent::TrowGrenade()
{
	if (Grenades == 0) return;

	if (CombatState != ECombatState::ECS_Unoccupied || EquippedWeapon == nullptr) return;

	CombatState = ECombatState::ECS_TrowhGrenade;
	if (Character)
	{
		Character->PlayThrowGrenadeMontage();
		AttachActorLeftHand(EquippedWeapon);
		ShowAttachGrenadeMesh(true);
	}
	if (!Character->HasAuthority())
	{
		ServerThrowGrenade();
	}
	if (Character->HasAuthority())
	{
		Grenades = FMath::Clamp(Grenades - 1, 0, MaxGrenades);
		UpdateHUDGrenades();
	}
}

void UCombatComponent::ServerThrowGrenade_Implementation()
{
	if (Grenades == 0) return;

	CombatState = ECombatState::ECS_TrowhGrenade;
	if (Character)
	{
		Character->PlayThrowGrenadeMontage();
		AttachActorLeftHand(EquippedWeapon);
		ShowAttachGrenadeMesh(true);
	}

	Grenades = FMath::Clamp(Grenades - 1, 0, MaxGrenades);
	UpdateHUDGrenades();
}

void UCombatComponent::TrowGrenadeFinish()
{
	CombatState = ECombatState::ECS_Unoccupied;
	AttachActorRightHand(EquippedWeapon);
}

void UCombatComponent::ReloadWeapon()
{
	if (CurriedAmmo > 0 && CombatState == ECombatState::ECS_Unoccupied && !EquippedWeapon->IsFullAmmo() && !bLocallyReload)
	{
		ServerReloadWeapon();
		HandleReloadWeapon();
		bLocallyReload = true;
	}
}



void UCombatComponent::ServerReloadWeapon_Implementation()
{
	if(Character == nullptr || EquippedWeapon == nullptr) return;
	CombatState = ECombatState::ECS_Reloading;
	
	if(!Character->IsLocallyControlled()) HandleReloadWeapon();
}

void UCombatComponent::DropEquipWeapon()
{
	if (EquippedWeapon)
	{
		EquippedWeapon->Dropped();
	}
}

void UCombatComponent::AttachActorRightHand(AActor* AttachActor)
{
	if (Character == nullptr || Character->GetMesh() == nullptr || AttachActor == nullptr) return;

	const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(FName(TEXT("RightHandSocket")));
	if (HandSocket)
	{
		HandSocket->AttachActor(AttachActor, Character->GetMesh());
	}
}

void UCombatComponent::AttachActorLeftHand(AActor* AttachActor)
{
	if (Character == nullptr || Character->GetMesh() == nullptr || AttachActor == nullptr) return;

	const USkeletalMeshSocket* HandSocket = Character->GetMesh()->GetSocketByName(FName(TEXT("LeftHandSocket")));
	if (HandSocket)
	{
		HandSocket->AttachActor(AttachActor, Character->GetMesh());
	}
}

void UCombatComponent::AttachActorToBackPack(AActor* AttachActor)
{
	if (Character == nullptr || Character->GetMesh() == nullptr || AttachActor == nullptr) return;

	const USkeletalMeshSocket* BackPackSocket = Character->GetMesh()->GetSocketByName(FName(TEXT("BackpackSocket")));
	if (BackPackSocket)
	{
		BackPackSocket->AttachActor(AttachActor, Character->GetMesh());
	}
}

void UCombatComponent::UpdateCurridAmmo()
{
	if (EquippedWeapon == nullptr) return;

	if (CarriedAmmoType.Contains(EquippedWeapon->GetWeaponType()))
	{
		CurriedAmmo = CarriedAmmoType[EquippedWeapon->GetWeaponType()];
	}

	Controller = Controller == nullptr ? Cast<ABlasterPlayerController>(Character->Controller) : Controller;
	if (Controller)
	{
		Controller->SetHUDCurriedAmmo(CurriedAmmo);
	}
}

void UCombatComponent::PlayEquipWeaponSound(AWeapon* WeaponToEquip)
{
	if (Character && WeaponToEquip && WeaponToEquip->EquipSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, WeaponToEquip->EquipSound, Character->GetActorLocation());
	}
}

void UCombatComponent::ReloaEmpthyWeapon()
{
	if (Character && EquippedWeapon && EquippedWeapon->IsEmpthy())
	{
		ReloadWeapon();
	}
}

void UCombatComponent::ShowAttachGrenadeMesh(bool bShowMesh)
{
	if (Character && Character->GetGrenadeMesh())
	{
		Character->GetGrenadeMesh()->SetVisibility(bShowMesh);
	}
}

void UCombatComponent::LauchGrenade()
{
	ShowAttachGrenadeMesh(false);

	if (Character && Character->IsLocallyControlled())
	{
		ServerLaunchGrenade(HitTarget);
	}
	
}

void UCombatComponent::ServerLaunchGrenade_Implementation(const FVector_NetQuantize& TargetHit)
{
	if (Character && GrenadeClass && Character->GetGrenadeMesh() && GrenadeClass)
	{
		const FVector StartLocation = Character->GetGrenadeMesh()->GetComponentLocation();
		FVector ToTarget = TargetHit - StartLocation;
		FActorSpawnParameters SpawnParam;
		SpawnParam.Owner = Character;
		SpawnParam.Instigator = Character;
		UWorld* World = GetWorld();
		if (World)
		{
			World->SpawnActor<AProjectile>(GrenadeClass, StartLocation, ToTarget.Rotation(), SpawnParam);
		}
	}
}

void UCombatComponent::EquipWeapon(class AWeapon* WeaponEquip)
{
	if(Character == nullptr || WeaponEquip == nullptr) return;
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	
	if (EquippedWeapon != nullptr && SecondaryWeapon == nullptr)
	{
		EquipSecondaryWeapon(WeaponEquip);
	}
	else
	{
		EquipPrimaryWeapon(WeaponEquip);
	}

	Character->GetCharacterMovement()->bOrientRotationToMovement = false;
	Character->bUseControllerRotationYaw = true;
}


void UCombatComponent::SwapWeapon()
{
	if (CombatState != ECombatState::ECS_Unoccupied && !Character) return;

	Character->PlaySwapWeaponMontage();
	Character->bFinishSwapping = false;
	CombatState = ECombatState::ECS_SwapWeapon;

	AWeapon* TempWeapon = EquippedWeapon;
	EquippedWeapon = SecondaryWeapon;
	SecondaryWeapon = TempWeapon;
	
}

void UCombatComponent::EquipPrimaryWeapon(AWeapon* WeaponToEquip)
{
	//Drop Weapon
	DropEquipWeapon();

	//Set Weapon Character and State Weapon
	EquippedWeapon = WeaponToEquip;
	EquippedWeapon->SetWeaponState(EWeaponState::EWS_Equpped);

	//Attach Actor Right Hand
	AttachActorRightHand(WeaponToEquip);

	//Set Owner Weapon and Update Hud Ammo
	EquippedWeapon->SetOwner(Character);
	EquippedWeapon->SetHUDAmmo();

	// Update Curried Ammo and Update Hud Curried Ammo

	UpdateCurridAmmo();

	//Play Equipped Sound
	PlayEquipWeaponSound(WeaponToEquip);

	//Reload Empthy Weapon

	ReloaEmpthyWeapon();
}

void UCombatComponent::EquipSecondaryWeapon(AWeapon* WeaponToEquip)
{
	SecondaryWeapon = WeaponToEquip;
	SecondaryWeapon->SetWeaponState(EWeaponState::EWS_EquppedSecondary);
	AttachActorToBackPack(WeaponToEquip);
	SecondaryWeapon->SetOwner(Character);
	PlayEquipWeaponSound(WeaponToEquip);
}

bool UCombatComponent::ShouldSwapWeapons()
{
	return EquippedWeapon != nullptr && SecondaryWeapon != nullptr;
}

void UCombatComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime,TickType,ThisTickFunction);

	
	if (Character && Character->IsLocallyControlled())
	{
		FHitResult HitResult;
		TraceUnderCrossHair(HitResult);
		HitTarget = HitResult.ImpactPoint;
		SetHUDCrossHair(DeltaTime);
		InterpFOV(DeltaTime);
	}
	
}





