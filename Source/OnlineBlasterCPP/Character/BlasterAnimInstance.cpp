// Fill out your copyright notice in the Description page of Project Settings.


#include "BlasterAnimInstance.h"
#include "BlasterCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "OnlineBlasterCPP/Weapon/Weapon.h"
#include "OnlineBlasterCPP/Components/CombatComponent.h"

void UBlasterAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	BlasterCharacter = Cast<ABlasterCharacter>(TryGetPawnOwner());
}

void UBlasterAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	if (BlasterCharacter == nullptr)
	{
		BlasterCharacter = Cast<ABlasterCharacter>(TryGetPawnOwner());
	}
	if(BlasterCharacter == nullptr) return;

	FVector Velocity = BlasterCharacter->GetVelocity();
	Velocity.Z = 0.f;
	Speed = Velocity.Size();

	bIsInAir = BlasterCharacter->GetCharacterMovement()->IsFalling();

	bIsAccelerating = BlasterCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0 ? true : false;

	bWeaponEquip = BlasterCharacter->IsWeaponEquip();

	EquippedWeapon = BlasterCharacter->GetEquippedWeapon();

	bIsCrouch = BlasterCharacter->bIsCrouched;

	bAiming = BlasterCharacter->IsAiming();

	TurningInPlace = BlasterCharacter->GetTurninginPlace();

	bRotateRootBone = BlasterCharacter->GetRotateRootBone();

	bElimed = BlasterCharacter->GetIsElim();

	// offSet Yaw 
	FRotator AimRotation = BlasterCharacter->GetBaseAimRotation();
	FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(BlasterCharacter->GetVelocity());
	FRotator DeltaRot = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation,AimRotation);
	DeltaRotationChar = FMath::RInterpTo(DeltaRotationChar,DeltaRot,DeltaSeconds,5.f);
	YawOffSet = DeltaRotationChar.Yaw;



	//Lean
	CharacterRotationLastFrame = CharacterRotation;
	CharacterRotation = BlasterCharacter->GetActorRotation();
	const FRotator DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation,CharacterRotationLastFrame);
	const float Target = DeltaRotation.Yaw / DeltaSeconds;
	const float Interp = FMath::FInterpTo(Lean,Target,DeltaSeconds,6.f);
	Lean = FMath::Clamp(Interp,-90.f,90.f);

	AO_Yaw = BlasterCharacter->GetYawAO();
	AO_Pitch = BlasterCharacter->GetPitchAO();
	

	if (bWeaponEquip && EquippedWeapon && EquippedWeapon->GetWeaponMesh() && BlasterCharacter->GetMesh())
	{
		LeftHandTransform = EquippedWeapon->GetWeaponMesh()->GetSocketTransform(FName(TEXT("LeftHandSocket")),ERelativeTransformSpace::RTS_World);
		FVector OutPosition;
		FRotator OutRotation;
		BlasterCharacter->GetMesh()->TransformToBoneSpace(FName("hand_r"),LeftHandTransform.GetLocation(),FRotator::ZeroRotator,OutPosition,OutRotation);
		LeftHandTransform.SetLocation(OutPosition);
		LeftHandTransform.SetRotation(FQuat(OutRotation));


		//Rotation Weapon Crosshair
		if (BlasterCharacter->IsLocallyControlled())
		{
			bIsLocalControllerCharacter = true;
			FTransform RightHandTransform = EquippedWeapon->GetWeaponMesh()->/*BlasterCharacter->GetMesh()->*/GetSocketTransform(FName(TEXT("hand_r")), ERelativeTransformSpace::RTS_World);
			FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(RightHandTransform.GetLocation(), RightHandTransform.GetLocation() + (RightHandTransform.GetLocation() - BlasterCharacter->GetHitTarget()));
			RightHandRotation = FMath::RInterpTo(RightHandRotation, LookAtRotation, DeltaSeconds, 30.f);
		}
	}

	bUseFABRIK = BlasterCharacter->GetCombatState() ==ECombatState::ECS_Unoccupied ;
	if (BlasterCharacter->IsLocallyControlled() && BlasterCharacter->GetCombatState() != ECombatState::ECS_TrowhGrenade && BlasterCharacter->bFinishSwapping)
	{
		bUseFABRIK = !BlasterCharacter->GetLocallyReloading();
	}
	bUseAimOffset = BlasterCharacter->GetCombatState() == ECombatState::ECS_Unoccupied && !BlasterCharacter->GetbDisableInputGame();
	bTransformRightHand = BlasterCharacter->GetCombatState() == ECombatState::ECS_Unoccupied && !BlasterCharacter->GetbDisableInputGame();
}
