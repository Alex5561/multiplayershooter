// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "OnlineBlasterCPP/TurninginPlace.h"
#include "OnlineBlasterCPP/Interface/InteractWithCrossHairInterface.h"
#include "Components/TimelineComponent.h"
#include "OnlineBlasterCPP/Components/CombatComponent.h"
#include "BlasterCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLeftGame);

UCLASS()
class ONLINEBLASTERCPP_API ABlasterCharacter : public ACharacter, public IInteractWithCrossHairInterface
{
	GENERATED_BODY()

public:
	
	FOnLeftGame OnLeftGame;

	UFUNCTION(Server,Reliable)
		void ServerLeftGame();

	ABlasterCharacter();

	virtual void Tick(float DeltaTime) override;


	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void OnRep_ReplicatedMovement() override;

	virtual void PostInitializeComponents() override;

	void PlayFireMontage(bool bAiming);
	void PlayHitReactMontage();
	void PlayElimMontageCharacter();
	void PlayReloadWeaponMontage();
	void PlayThrowGrenadeMontage();
	void PlaySwapWeaponMontage();

	bool bFinishSwapping = false;

	/*UFUNCTION(NetMulticast,Unreliable)
		void MulticastHit();*/
	
	void Elim(bool bPlayerLeftGame);
	UFUNCTION(NetMulticast,Reliable)
	void MulticastElim(bool bPlayerLeftGame);


	virtual void Destroyed() override;

	UPROPERTY(Replicated)
	bool bDisableGamePlay = false;

	UFUNCTION(BlueprintImplementableEvent)
		void ShowSniperScopeWidget(bool bShowScope);

	void UpdateHUDHealth();
	void UpdateHUDShield();
	void UpdateHUDAmmoWeapon();

	void SpawnWeaponDef();

	TMap<FName, class UBoxComponent* >HitCollisionBoxes;


	UFUNCTION(NetMulticast,Reliable)
		void MulticastGainedTheLead();
	UFUNCTION(NetMulticast,Reliable)
		void MulticastLostTheLead();

	void SetTeamColor(ETeam Team);
	
protected:
	

	/**
	* Hit boxes used for server-side rewind
	*/

	UPROPERTY(EditAnywhere)
		class UBoxComponent* head;

	UPROPERTY(EditAnywhere)
		UBoxComponent* pelvis;

	UPROPERTY(EditAnywhere)
		UBoxComponent* spine_02;

	UPROPERTY(EditAnywhere)
		UBoxComponent* spine_03;

	UPROPERTY(EditAnywhere)
		UBoxComponent* upperarm_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* upperarm_r;

	UPROPERTY(EditAnywhere)
		UBoxComponent* lowerarm_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* lowerarm_r;

	UPROPERTY(EditAnywhere)
		UBoxComponent* hand_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* hand_r;

	UPROPERTY(EditAnywhere)
		UBoxComponent* backpack;

	UPROPERTY(EditAnywhere)
		UBoxComponent* blanket;

	UPROPERTY(EditAnywhere)
		UBoxComponent* thigh_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* thigh_r;

	UPROPERTY(EditAnywhere)
		UBoxComponent* calf_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* calf_r;

	UPROPERTY(EditAnywhere)
		UBoxComponent* foot_l;

	UPROPERTY(EditAnywhere)
		UBoxComponent* foot_r;


	

	virtual void BeginPlay() override;


	void MoveForward(float value);
	void MoveRight(float value);
	void Turn(float value);
	void LookUP(float value);

	void EquipButtonPressed();
	void CrouchButtonPressed();

	void AimButtonPressed();
	void AimButtonReleased();

	void GrenadeButtonPressed();

	void ReloadButtonPressed();

	void AimOffSet(float DeltaTime);

	void CalculateAO_PitchOffSet();
	
		
	

	void SimProxyTurn();
	void TurnInPlace(float DeltaTime);
	virtual void Jump() override;

	void FireButtonPressed();
	void FireButtonReleased();

	void HideCameraIfCharacterClose();
	UFUNCTION()
	void ReceiveDamage(AActor* DamageActor,float Damage,const UDamageType* DamageType,class AController* InstigatorController,AActor* DamageCauser);

	void DropOnDestroyWeapon(AWeapon* Weapon);

	void PollInit();
private:

	UPROPERTY(VisibleAnywhere, Category = "Camera")
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, Category = "Camera")
		class UCameraComponent* FollowCamera;
	UPROPERTY(ReplicatedUsing = OnRep_OverlapWeapon)
		class AWeapon* OverlapWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,meta = (AllowPrivateAccess = "true"))
		class UCombatComponent* CombatComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UBuffComponent* BuffComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class ULafCompensasionComponent* LagCompensationComponent;

	UFUNCTION()
		void OnRep_OverlapWeapon(AWeapon* LastOverlapWeapon);
	UFUNCTION(Server,Reliable)
		void ServerEquipButtonPressed();
	
	float AO_Yaw;
	float InterpAO_Yaw;
	float AO_Pitch;
	FRotator StartingAimRotation;

	ETurninginPlace TurninginPlace;

	UPROPERTY(EditAnywhere,Category = "AnimMontage")
		class UAnimMontage* FireWeaponMontage;
	UPROPERTY(EditAnywhere, Category = "AnimMontage")
		class UAnimMontage* HitMontage;
	UPROPERTY(EditAnywhere, Category = "AnimMontage")
		class UAnimMontage* ElimMontage;
	UPROPERTY(EditAnywhere, Category = "AnimMontage")
		class UAnimMontage* ReloadMontage;
	UPROPERTY(EditAnywhere, Category = "AnimMontage")
		class UAnimMontage* GrenadeMontage;
	UPROPERTY(EditAnywhere, Category = "AnimMontage")
		class UAnimMontage* SwapMontage;

	//ProxyMovement
		bool bRotateRootBone;
		float TurnTreshold = 0.5f;
		FRotator ProxyRotationLastFrame;
		FRotator ProxyRotation;
		float ProxyYaw;
		float TimeSenseLastMovementReplication;

	//Player Health

	UPROPERTY(EditAnywhere,Category = "PlayerStats")
		float MaxHealth = 100.f;
	UPROPERTY(ReplicatedUsing = OnRep_Health,VisibleAnywhere,Category = "PlayerStats")
		float Health = 100.f;

	//Shield
	UPROPERTY(EditAnywhere, Category = "PlayerStats")
		float MaxShield = 100.f;
	UPROPERTY(ReplicatedUsing = OnRep_Shield, VisibleAnywhere, Category = "PlayerStats")
		float Shield = 100.f;

	UFUNCTION()
		void OnRep_Shield(float LastShield);

	UFUNCTION()
		void OnRep_Health(float LastHealth);
	UPROPERTY()
		class ABlasterPlayerController* BlasterPlayerController;

		bool bElim = false;

		bool bLeftGame = false;

		FTimerHandle ElimTimerHandle;
		void FinishElimTimer();
	UPROPERTY(EditDefaultsOnly)
		float ElimDelay = 3.f;


	//Desolv Character

	UPROPERTY(VisibleAnywhere)
		UTimelineComponent* DissolveTimeLine;

	FOnTimelineFloat DissolveTrack;

	UFUNCTION()
		void UpdateDissolveMaterials(float Dissolve);
		void StartDissolves();

	UPROPERTY(EditAnywhere)
		UCurveFloat* CurveDissolv;

	UPROPERTY(VisibleAnywhere,Category = "DynamicMaterial")
		UMaterialInstanceDynamic* DynamicMaterialDessolv;
	UPROPERTY(EditAnywhere, Category = "DynamicMaterial")
		UMaterialInstance* MaterialDessolv;

	//Team Material

	UPROPERTY(EditAnywhere, Category = "DynamicMaterial")
		UMaterialInstance* RedMaterial;
	UPROPERTY(EditAnywhere, Category = "DynamicMaterial")
		UMaterialInstance* BlueMaterial;

	// Elim Bot
	UPROPERTY(EditAnywhere)
		UParticleSystem* ElimBotEffect;
	UPROPERTY(VisibleAnywhere)
		UParticleSystemComponent* ElimFXComponent;

	UPROPERTY(EditAnywhere)
		class USoundCue* ElimBotSound;

	//Crown

	UPROPERTY(EditAnywhere)
		class UNiagaraSystem* CrownSystem;

	UPROPERTY()
		class UNiagaraComponent* CrownComponent;

	//PlayerState
	UPROPERTY()
		class ABlasterPlayerState* PlayerStateBlaster;

	//DisableInput Rotate
	void RotateInPlace(float DeltaTime);


	//Grenade 

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* GrenadeMesh;


//Default Weapon

	UPROPERTY(EditAnywhere)
		TSubclassOf<AWeapon>DefaultWeapon; 

	UPROPERTY()
		class ABlasterGameMode* BlasterGameMode;

public:

	 void SetOverlapWeapon(AWeapon* Weapon); 
	 bool IsWeaponEquip();
	 bool IsAiming();

	 FORCEINLINE float GetYawAO() const {return AO_Yaw;}
	 FORCEINLINE float GetPitchAO() const { return AO_Pitch; }
	  AWeapon* GetEquippedWeapon();
	  FORCEINLINE ETurninginPlace GetTurninginPlace() const {return TurninginPlace;}
	  FVector GetHitTarget() const;
	  FORCEINLINE UCameraComponent* GetFollowCamera() const {return FollowCamera;}
	  FORCEINLINE bool GetRotateRootBone() const {return bRotateRootBone;}
	  FORCEINLINE bool GetIsElim() const {return bElim;}
	  FORCEINLINE float GetHealth() const {return Health;}
	  FORCEINLINE void SetHEalth(float Amount) { Health = Amount; }
	  FORCEINLINE float GetMaxHealth() const { return MaxHealth; }
	  ECombatState GetCombatState() const;
	  FORCEINLINE UCombatComponent* GetCombatComponent() const { return CombatComponent; }
	  FORCEINLINE bool GetbDisableInputGame() const { return bDisableGamePlay; }
	  FORCEINLINE UAnimMontage* GetReloadMontage() const { return ReloadMontage; }
	  FORCEINLINE UStaticMeshComponent* GetGrenadeMesh() const { return GrenadeMesh; }
	  FORCEINLINE UBuffComponent* GetBuffComponent() const { return BuffComponent; }
	  FORCEINLINE bool GetElim() const { return bElim; }
	  FORCEINLINE float GetShield() const { return Shield; }
	  FORCEINLINE float GetMaxShield() const { return MaxShield; }
	  FORCEINLINE void SetShield(float Shieldvalue) { Shield = Shieldvalue; }
	  bool GetLocallyReloading();
	  FORCEINLINE ULafCompensasionComponent* GetLagComponent() const { return LagCompensationComponent; }
};
